//
//  EmailVerificationView.swift
//  EduLights
//
//  Created by Abdul Muqeem on 16/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class EmailVerificationView: UIView {
    
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var lblMessage:UILabel!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtToken:UITextField!
    @IBOutlet weak var btnOk:UIButton! // Single
    
    var view: UIView!
    var delegate:EmailVerificationViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        xibSetup()
    }
    
    
    func xibSetup(){
        
        backgroundColor = .clear
        view = loadViewFromNib()
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.translatesAutoresizingMaskIntoConstraints = true
        addSubview(view)
        
    }
    
    
    func loadViewFromNib() -> UIView{
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let nibView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        
        return nibView
        
    }
    
    
    @IBAction func okAction(_ sender : UIButton) {
        self.delegate?.doneAction()
    }
    
}

