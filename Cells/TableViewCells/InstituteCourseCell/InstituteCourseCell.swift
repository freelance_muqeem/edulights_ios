//
//  InstituteCourseCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 15/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class InstituteCourseCell: UITableViewCell {

    @IBOutlet weak var lblPrice:UILabel!
    @IBOutlet weak var lblCourseName:UILabel!
    @IBOutlet weak var lblTeacherName:UILabel!
    @IBOutlet weak var lblDate:UILabel!
    
    @IBOutlet weak var shadowView:UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
