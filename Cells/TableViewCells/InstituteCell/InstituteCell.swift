//
//  InstituteCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 10/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class InstituteCell: UITableViewCell {

    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblAddress:UILabel!
    
    static let ID = "ExpandableCell"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
