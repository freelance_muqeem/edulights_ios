//
//  InstituteDetailsHeaderView.swift
//  EduLights
//
//  Created by Abdul Muqeem on 13/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ExpandableCell
import ExpyTableView


class InstituteDetailsHeaderView: UITableViewCell , ExpyTableViewHeaderCell {
    
    @IBOutlet weak var lblTitle:UILabel!
    @IBOutlet weak var imgArrow:UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
            
        case .willExpand:
            print("WILL EXPAND")
        case .willCollapse:
            print("WILL COLLAPSE")
        case .didExpand:
            self.imgArrow.image = UIImage(named: "downword_icon")!
        case .didCollapse:
            self.imgArrow.image = UIImage(named: "forword_icon")!
        }
    }
    
}


