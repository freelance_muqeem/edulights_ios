//
//  FoodMyCartCell.swift
//  WAAFI
//
//  Created by Abdul Muqeem on 08/08/2018.
//  Copyright © 2018 Safarfone. All rights reserved.
//

import UIKit

class CourseCell: UITableViewCell {
    
    @IBOutlet weak var collectionView:UICollectionView!
    @IBOutlet weak var lblName:UILabel!
    
    var MyCourseListArray:[Course]? = [Course]()
    
    var indexpath:IndexPath?
    var navigationDelegate: NavigationDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //register cell
        let nib = UINib(nibName:String(describing:MyCourseCollectionCell.self), bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier:String(describing: MyCourseCollectionCell.self))
        
    }
    
    
}

extension CourseCell : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.MyCourseListArray!.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let screenWidth =  collectionView.bounds.size.width - 20
        let height = GISTUtility.convertToRatio(120)
        return CGSize(width: screenWidth/1 , height: height)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let obj = self.MyCourseListArray![indexPath.row]
        
        let  cell:MyCourseCollectionCell = collectionView.dequeueReusableCell(withReuseIdentifier: String(describing:MyCourseCollectionCell.self), for: indexPath) as! MyCourseCollectionCell
        
        //        cell.layer.shadowColor = UIColor.lightGray.cgColor
        //        cell.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        //        cell.layer.shadowRadius = 1
        //        cell.layer.shadowOpacity = 0.9
        //        cell.layer.masksToBounds = false
        //        cell.layer.shadowPath = UIBezierPath(roundedRect: cell.bounds, cornerRadius: cell.contentView.layer.cornerRadius).cgPath
        
        if let courseName = obj.name {
            let courseNum = obj.num
            if courseNum == 0 {
                cell.lblCourseName.text = courseName
            } else {
                cell.lblCourseName.text = courseName  + " \(courseNum!)"
            }
        }
        
        if let instituteName = obj.institute!.name {
            cell.lblInstituteName.text = instituteName
        }
        
        if let logo = obj.institute!.logoImage {
            cell.imgLogo.setImageFromUrl(urlStr: logo)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let obj = self.MyCourseListArray![indexPath.row]
        self.navigationDelegate?.didNavigate(course: obj)
    }
}



