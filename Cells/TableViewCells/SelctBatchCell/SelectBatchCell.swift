//
//  SelectBatchCell.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

class SelectBatchCell: UITableViewCell {

    @IBOutlet weak var lblBatchNo:UILabel!
    @IBOutlet weak var lblStartDate:UILabel!
    @IBOutlet weak var lblEndDate:UILabel!
    @IBOutlet weak var lblTimeMonday:UILabel!
    @IBOutlet weak var lblTimeTuesday:UILabel!
    @IBOutlet weak var lblTimeWednesday:UILabel!
    @IBOutlet weak var lblTimeThursday:UILabel!
    @IBOutlet weak var lblTimeFriday:UILabel!
    @IBOutlet weak var lblTimeSaturday:UILabel!
    @IBOutlet weak var lblTimeSunday:UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
