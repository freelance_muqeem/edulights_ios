//
//  SideMenuViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/09/2018.
//  Copyright © 2018 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SideMenuViewController: AlertViewDelegate {
    
    func okAction() {
        
        self.alertView.isHidden = true
    }
    
}

class SideMenuViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SideMenuViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SideMenuViewController
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var btnVerifyEmail:UIButton!
    
    @IBOutlet weak var imgLogo:UIImageView!
    @IBOutlet weak var lblName:UILabel!
    @IBOutlet weak var lblDegree:UILabel!
    
    var expireValue:String?
    let currentDate = Date()
    
    var itemsLogin: [String] = [ "My Courses" , "My Profile" , "Notifications" , "Institutes" , "Settings" , "Logout"]
    var imagesLogin: [UIImage] = [UIImage(named:"book_icon")! , UIImage(named:"profile_icon")! , UIImage(named:"notification_icon")! , UIImage(named:"institute_icon")! , UIImage(named:"setting_icon")! , UIImage(named:"logout_icon")! ]
    
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        //Register Cell
        let cell = UINib(nibName:String(describing:SideMenuTableViewCell.self), bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: String(describing: SideMenuTableViewCell.self))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileUpdate), name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getEmailUpdate), name: NSNotification.Name(rawValue: EMAILUPDATED), object: nil)
        
        self.getEmailUpdate()
        self.getProfileUpdate()

    }
    
    @objc func getEmailUpdate() {
        
        let value = UserDefaults.standard.string(forKey: "Email_Verified")
        if value == "true" {
            self.btnVerifyEmail.setTitleColor(UIColor.init(rgb: 0x78C395), for: .normal)
            self.btnVerifyEmail.setTitle("Email is Verified", for: .normal)
            self.btnVerifyEmail.isUserInteractionEnabled = false
            return
        }
        
        if Singleton.sharedInstance.CurrentUser!.isVerify == 0 {
            self.btnVerifyEmail.setTitleColor(UIColor.init(rgb: 0xff0000), for: .normal)
            self.btnVerifyEmail.setTitle("Verify Your Email", for: .normal)
            self.btnVerifyEmail.isUserInteractionEnabled = true
        }
        else {
            self.btnVerifyEmail.setTitleColor(UIColor.init(rgb: 0x78C395), for: .normal)
            self.btnVerifyEmail.setTitle("Email is Verified", for: .normal)
            self.btnVerifyEmail.isUserInteractionEnabled = false
        }
    }
    
    @objc func getProfileUpdate() {
        
        if let image = Singleton.sharedInstance.CurrentUser!.profileImage {
            self.imgLogo.setImageFromUrl(urlStr: image)
        }
        
        if let firstName = Singleton.sharedInstance.CurrentUser!.firstName {
            let lastName = Singleton.sharedInstance.CurrentUser!.lastName!
            self.lblName.text = firstName + " " + lastName
        }
        
        if let education = Singleton.sharedInstance.CurrentUser!.educationType {
            self.lblDegree.text = education
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func verifyEmail(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let email = Singleton.sharedInstance.CurrentUser!.email!
        
        let params:[String:Any] = ["email":email]
        print(params)
        
        HomeServices.SendEmailToken(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let navigationController = self.sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            vc.isEmail = true
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        })

    }
    
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.itemsLogin.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(60)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: SideMenuTableViewCell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableViewCell") as! SideMenuTableViewCell
        
        cell.lblTitle.text = self.itemsLogin[indexPath.row]
        cell.imgView.image = self.imagesLogin[indexPath.row]
        
        if indexPath.row == 6 {
            cell.lineView.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 1 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = MyProfileViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 2 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = NotificationViewController.instantiateFromStoryboard()
            vc.isOpenSideMenu = true
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        } else if indexPath.row == 3 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = InstitutesListViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        }  else if indexPath.row == 4 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = SettingsViewController.instantiateFromStoryboard()
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        }  else if indexPath.row == 5 {
            
            let navigationController = sideMenuController?.rootViewController as! UINavigationController
            let vc = HomeViewController.instantiateFromStoryboard()
            vc.isLogout = true
            navigationController.setViewControllers([vc], animated: false)
            self.hideLeftViewAnimated(self)
            
        }
        
    }
    
}





