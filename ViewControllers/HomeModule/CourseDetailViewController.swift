//
//  CourseDetailViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 15/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension CourseDetailViewController : AlertViewDelegate , AlertViewDelegateAction {
    
    func okAction() {
        if self.textAlert == "Send" {
            self.navigationController?.popToRootViewController(animated: true)
        }
        self.alertView.isHidden = true
    }
    
    func okButtonAction() {
        self.sendRequest()
    }
    
    func cancelAction() {
        self.alertView.isHidden = true
    }
    
}

class CourseDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> CourseDetailViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! CourseDetailViewController
    }
    
    //Teacher View
    @IBOutlet weak var imgTeacher:UIImageView!
    @IBOutlet weak var lblTeacherName:UILabel!
    @IBOutlet weak var lblTeacherCourses:UILabel!
    @IBOutlet weak var lblTeacherFees:UILabel!
    
    @IBOutlet weak var lblCourseDescription:UILabel!
    @IBOutlet weak var lblAdditionalDetails:UILabel!
    @IBOutlet weak var txtBatch:UITextField!
    
    @IBOutlet weak var lblError:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var InstituteId:Int = 0
    var courseId:Int = 0
    var batchId:Int = 0
    var courseObj:Course!
    
    var resultObj:CourseDetail!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.courseObj.name
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.delegateAction = self
        self.alertView.isHidden = true
        
        self.lblError.isHidden = true
        
        self.GetCourseDetail()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK :- Array Name Comma Seperated
    
    func courseStrAppendWithComa(courseArray:[TeacherCourses]) -> String {
        
        var courseTitleArray:[String] = [String]()
        for obj in courseArray {
            courseTitleArray.append(obj.name!)
        }
        
        return courseTitleArray.joined(separator: ",")
        
    }
    
    //MARK:-  Course Detail Service Calling
    
    func GetCourseDetail() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id , "course_id":self.courseId]
        print(params)
        
        HomeServices.GetCourseDetail(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.resultObj = CourseDetail(object:(response?["Result"])!)
            
            if let teacherImage = self.resultObj.teacher!.profileImage {
                self.imgTeacher.setImageFromUrl(urlStr: teacherImage)
            }
            
            if let teacherName = self.resultObj.teacher!.name {
                self.lblTeacherName.text = teacherName
            }
            
            for a in self.resultObj.teacher!.courses! {
                self.lblTeacherCourses.text = self.courseStrAppendWithComa(courseArray: [a])
            }
            
            if let teacherFees = self.resultObj.fees {
                self.lblTeacherFees.text = "PKR " + teacherFees
            }
            
            if let description = self.resultObj.descriptionValue {
                self.lblCourseDescription.text = description
            }
            
            if let additionalDetails = self.resultObj.additionalDetail {
                self.lblAdditionalDetails.text = additionalDetails
            }
            
            if let error = self.resultObj.aptitude {
                if error == "1" {
                    self.lblError.isHidden = false
                }
            }
            
        })
        
    }
    
    @IBAction func selectBatchAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        let vc = SelectBatchViewController.instantiateFromStoryboard()
        vc.courseId = self.courseId
        vc.delegate = self
        let navVc = UINavigationController(rootViewController: vc)
        self.present(navVc, animated:true, completion: nil)
        
    }
    
    @IBAction func teacherAction( _ sender : UIButton) {
        
        let vc = TeacherProfileViewController.instantiateFromStoryboard()
        vc.resultObj = self.resultObj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func sendRequestAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        
        if self.txtBatch.text!.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Kindly select batch", style: .danger)
            return
        }
        
        self.alertView.btnOkAction.setTitle("Yes", for: .normal)
        self.alertView.btnCancel.setTitle("No", for: .normal)
        
        self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "By sending a request for this course you accept and agree to the terms and conditions of the institute ?", id: 1)
        self.alertView.isHidden = false
        
    }
    
    func sendRequest() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id , "institute_id": self.InstituteId , "teacher_id":self.courseObj.teacher!.id! , "course_id":self.courseId , "batch_id":self.batchId]
        print(params)
        
        HomeServices.SendCourseRequest(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let message = "Thank you for purchasing course \(self.courseObj.name!) taught by \(self.resultObj.teacher!.name!). You will recieve a notification when your request is approved by the institute"
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: message , id: 0)
            self.alertView.isHidden = false
            self.textAlert = "Send"
            
        })
    }
    
}

extension CourseDetailViewController : GetBatchDelegate {
    
    func getBatch(obj: Batch) {
        
        if let id = obj.id {
            self.batchId = id
            print("Batch Id: \(id)")
        }
        
        if let batchNo = obj.batchNo {
            self.txtBatch.text = "Batch No : \(batchNo)"
        }
    }
    
}

