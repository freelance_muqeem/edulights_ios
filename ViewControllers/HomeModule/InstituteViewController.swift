//
//  InstituteViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 10/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension InstituteViewController : AlertViewDelegate {
    
    func okAction() {
        
        self.alertView.isHidden = true
    }
    
}

class InstituteViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> InstituteViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InstituteViewController
    }
    
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var lblIntro:UILabel!
    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var lblNoData:UILabel!
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    @IBOutlet weak var tblView:UITableView!
    
    var typeId:Int = 0
    var searchText:String = ""
    var bannerImage:String = ""
    
    var InstituteListArray:[InstituteData]? = [InstituteData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Institute"
        self.PlaceNavigationButtons(selectorForLeft: #selector(backAction) , leftImage: BACK_IMAGE , selectorForRight: #selector(filterAction), rightImage: FILTER_IMAGE)
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        if self.bannerImage != ""  {
            self.imgBanner.setImageFromUrl(urlStr: self.bannerImage)
        }
        if self.searchText != "" {
            self.txtSearch.text = self.searchText
        }
        
        self.GetInstituteList()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:InstituteCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: InstituteCell.self))
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func filterAction() {
        let vc = FilterViewController.instantiateFromStoryboard()
        vc.instituteText = self.searchText
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func searchAction( _ sender : UIButton) {
        
        self.InstituteListArray?.removeAll()
        self.tblView.reloadData()
        
        if self.txtSearch.text!.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Search can't be empty", style: .danger)
            return
        }
        
        self.view.endEditing(true)
        self.GetInstituteList()
        
    }
    
    //MARK:-  Institute Listing Service Calling
    
    func GetInstituteList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let keyword = self.txtSearch.text!
        self.searchText = keyword
        
        let params:[String:Any] = ["user_id":id , "keyword":keyword , "code":self.typeId]
        print(params)
        
        HomeServices.GetInstitute(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let count = response?["Result"]["count"].int {
                
                if count == 0 {
                    self.lblIntro.isHidden = true
                    self.tblView.isHidden = true
                    self.lblNoData.isHidden = false
                    return
                }
                
                if self.typeId == 1 {
                    self.lblIntro.text = "\(count) filter results for \(self.searchText)"
                }
                else {
                    self.lblIntro.text = "\(count) Institutes offering a \(self.searchText) course"
                }
                
                self.lblIntro.isHidden = false
                self.tblView.isHidden = false
                self.lblNoData.isHidden = true
                
            }
            
            let instituteResponseArray = response?["Result"]["data"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  InstituteData(json: resultObj)
                self.InstituteListArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
}

extension InstituteViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.InstituteListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(130)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:InstituteData = self.InstituteListArray![indexPath.row]
        
        let cell:InstituteCell = tableView.dequeueReusableCell(withIdentifier:String(describing:InstituteCell.self)) as!  InstituteCell
        
        if let image = obj.logoImage {
            cell.imgLogo.setImageFromUrl(urlStr: image)
        }
        
        if let name = obj.name {
            cell.lblName.text = name
        }
        
        if let address = obj.address {
            cell.lblAddress.text = address
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj:InstituteData = self.InstituteListArray![indexPath.row]
        
        let vc = InstituteDetailsViewController.instantiateFromStoryboard()
        vc.instituteId = obj.id!
        vc.instituteName = obj.name!
        vc.bannerImage = self.bannerImage
        if self.typeId == 2 {
            vc.courseName = self.searchText
        }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

extension InstituteViewController : GetFilterInstitute {
    
    func getFilter(data: [InstituteData]) {
        
        self.InstituteListArray?.removeAll()
        self.InstituteListArray = data
        
        if self.InstituteListArray!.count == 0 {
            self.lblIntro.isHidden = true
            self.tblView.isHidden = true
            self.lblNoData.isHidden = false
            return
        }
        else {
            self.lblIntro.isHidden = false
            self.lblNoData.isHidden = true
            self.tblView.isHidden = false
            self.tblView.reloadData()
        }
        
    }
    
}

