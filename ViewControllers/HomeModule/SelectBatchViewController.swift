//
//  SelectBatchViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SelectBatchViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}


class SelectBatchViewController: UIViewController {
    
    
    class func instantiateFromStoryboard() -> SelectBatchViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SelectBatchViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var lblNoData:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var courseId:Int = 0
    var BatchListArray:[Batch]? = [Batch]()
    var delegate:GetBatchDelegate!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Select Batch"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.GetBatchList()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:SelectBatchCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: SelectBatchCell.self))
        
    }
    
    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-  Batch Listing Service Calling
    
    func GetBatchList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id , "course_id":self.courseId]
        print(params)
        
        HomeServices.GetBatchDetail(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let instituteResponseArray = response?["Result"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  Batch(json: resultObj)
                self.BatchListArray?.append(obj)
            }
            
            if self.BatchListArray!.count == 0 {
                self.tblView.isHidden = true
                self.lblNoData.isHidden = false
            }
            else {
                
                self.tblView.isHidden = false
                self.lblNoData.isHidden = true
                self.tblView.reloadData()
            }
            
        })
        
    }
    
}


extension SelectBatchViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.BatchListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(280)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:Batch = self.BatchListArray![indexPath.row]
        
        let cell:SelectBatchCell = tableView.dequeueReusableCell(withIdentifier:String(describing:SelectBatchCell.self)) as!  SelectBatchCell
        
        if let batch = obj.batchNo {
            cell.lblBatchNo.text = "Batch No : \(batch)"
        } else {
            cell.lblBatchNo.text = "Batch No : N/A"
        }
        
        if let startDate = obj.startDate {
            cell.lblStartDate.text = "Start Date : \(startDate)"
        } else {
            cell.lblStartDate.text = "Start Date :   N/A"
        }
        
        if let endDate = obj.endDate {
            cell.lblEndDate.text = "End Date : \(endDate)"
        } else {
            cell.lblEndDate.text = "End Date : N/A"
        }
        
        if let days = obj.batchdays {
            
            for batch in days {
                if batch.day == "Monday" {
                    cell.lblTimeMonday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Tuesday" {
                    cell.lblTimeTuesday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Wednesday" {
                    cell.lblTimeWednesday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Thursday" {
                    cell.lblTimeThursday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Friday" {
                    cell.lblTimeFriday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Saturday" {
                    cell.lblTimeSaturday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Sunday" {
                    cell.lblTimeSunday.text = "Time : \(batch.toTime!) to \(batch.fromTime!)"
                }
            }
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            let obj:Batch = self.BatchListArray![indexPath.row]
            self.delegate.getBatch(obj: obj)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
}




