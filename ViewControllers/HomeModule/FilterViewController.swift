//
//  FilterViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 15/07/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown
import ActionSheetPicker_3_0

extension FilterViewController : AlertViewDelegate {
    
    func okAction() {
        
        self.alertView.isHidden = true
    }
    
}

class FilterViewController: UIViewController {

    class func instantiateFromStoryboard() -> FilterViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! FilterViewController
    }
    
    @IBOutlet weak var txtLocation:UITextField!
    @IBOutlet weak var txtStartDate:UITextField!
    @IBOutlet weak var txtEndDate:UITextField!
    @IBOutlet weak var txtInstitute:UITextField!
    @IBOutlet weak var txtStartTime:UITextField!
    @IBOutlet weak var txtEndTime:UITextField!
    @IBOutlet weak var txtMaxPrice:UITextField!
    @IBOutlet weak var txtMinPrice:UITextField!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var instituteText:String = ""
    
    var InstituteArray:[InstituteData]? = [InstituteData]()
    var locationListArray:[LocationData]? = [LocationData]()
    var InstituteListArray:[InstituteData]? = [InstituteData]()
    
    var locationDropDown = DropDown()
    var instituteDropDown = DropDown()
    
    var delegate:GetFilterInstitute!
    var locationID:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Filter"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.GetLocationList()
        self.GetInstituteList()
        
        // Location DropDown
        locationDropDown.anchorView = txtLocation // UIView or UIBarButtonItem
        locationDropDown.bottomOffset = CGPoint(x: 0, y:(locationDropDown.anchorView?.plainView.bounds.height)!)
        locationDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtLocation.text = item
            
            if let id = self.locationListArray![index].id {
                self.locationID = id
            }
            
        }
        
        // Institute DropDown
        instituteDropDown.anchorView = txtInstitute // UIView or UIBarButtonItem
        instituteDropDown.bottomOffset = CGPoint(x: 0, y:(instituteDropDown.anchorView?.plainView.bounds.height)!)
        instituteDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.txtInstitute.text = item
            
        }
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Institute Listing Service Calling
    
    func GetInstituteList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id]
        print(params)
        
        HomeServices.InstituteList(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let instituteResponseArray = response?["Result"].arrayValue
            var type = [String]()
    
            for resultObj in instituteResponseArray! {
                let obj =  InstituteData(json: resultObj)
                self.InstituteArray?.append(obj)
                type.append((obj.name)!)
            }
            
            self.instituteDropDown.dataSource = type
        })
        
    }

    //MARK:-  Location Listing Service Calling
    
    func GetLocationList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id]
        print(params)
        
        HomeServices.LocationList(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let instituteResponseArray = response?["Result"].arrayValue
            var type = [String]()
            
            for resultObj in instituteResponseArray! {
                let obj =  LocationData(json: resultObj)
                self.locationListArray?.append(obj)
                type.append((obj.area)!)
            }
            
            self.locationDropDown.dataSource = type
        })
        
    }

    
    @IBAction func locationAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.locationDropDown.show()
    }
    
    @IBAction func startDateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Start Date", datePickerMode: .date , selectedDate: Date(),  doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtStartDate.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func endDateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "End Date", datePickerMode: .date , selectedDate: Date(), doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtEndDate.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func instituteAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.instituteDropDown.show()
    }
    
    @IBAction func startTimeAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Start Time", datePickerMode: .time , selectedDate: Date(),  doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtStartTime.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
    }
    
    @IBAction func endTimeAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "End Time", datePickerMode: .time , selectedDate: Date(),  doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtEndTime.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
    @IBAction func filterAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        self.locationID = (self.locationID == 0) ? 0 : self.locationID
        let institute = (self.txtInstitute.text == "") ? "" : self.txtInstitute.text!
        let startDate = (self.txtStartDate.text == "") ? "" : self.txtStartDate.text!
        let endDate = (self.txtEndDate.text == "") ? "" : self.txtEndDate.text!
        let startTime = (self.txtStartTime.text == "") ? "" : self.txtStartTime.text!
        let endTime = (self.txtEndTime.text == "") ? "" : self.txtEndTime.text!
        let maxPrice = (self.txtMaxPrice.text == "") ? "" : self.txtMaxPrice.text!
        let minPrice = (self.txtMinPrice.text == "") ? "" : self.txtMinPrice.text!
        
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        
        let params:[String:Any] = ["user_id":id , "location_id":self.locationID , "institute":institute , "max_price":maxPrice , "min_price":minPrice , "start_date":startDate , "end_date":endDate , "start_time":startTime , "end_time":endTime]
        print(params)
        
        HomeServices.FilterList(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if response?["Result"]["count"].int == 0 {
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: "No Istitute Found" , id: 0)
                self.alertView.isHidden = false
                return
            }
            
            let instituteResponseArray = response?["Result"]["data"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  InstituteData(json: resultObj)
                self.InstituteListArray?.append(obj)
            }
            
            self.delegate.getFilter(data: self.InstituteListArray!)
            self.navigationController?.popViewController(animated: true)
            
        })
    }
}
