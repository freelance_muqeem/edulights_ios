//
//  InstituteDetailsViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 12/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ExpandableCell
import ExpyTableView

extension InstituteDetailsViewController : AlertViewDelegate {
    
    func okAction() {
        
        self.alertView.isHidden = true
    }
    
}

class InstituteDetailsViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> InstituteDetailsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InstituteDetailsViewController
    }
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var index:Int = 0
    var section: Int!

    @IBOutlet weak var imgBanner:UIImageView!
    @IBOutlet weak var txtAbout:UITextView!
    @IBOutlet weak var aboutView:UIView!
    @IBOutlet weak var fbLinkView:UIView!
    
    @IBOutlet weak var emptyView:UIView!
    @IBOutlet weak var tblView:ExpyTableView!
    
    @IBOutlet weak var btnCourse:UIButton!
    @IBOutlet weak var btnAbout:UIButton!
    @IBOutlet weak var btnFBLink:UIButton!
    
    var InstituteDetailArray:[Course]? = [Course]()
    
    var instituteId:Int = 0
    var instituteName:String = ""
    var bannerImage:String = ""
    var courseName:String = ""
    
    var objCourse:[Course]? = []
    
    var data: [[Course]] = []
    var i = [[Course]]()
    var temp = [[Course]]()
    var selectedInsitute: Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = self.instituteName
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.aboutView.isHidden = true
        self.fbLinkView.isHidden = true
        self.imgBanner.isHidden = false
        self.tblView.isHidden = false
        
        self.btnCourse.backgroundColor = UIColor.init(rgb: 0x78C395)
        self.btnAbout.backgroundColor = UIColor.white
        
        self.btnCourse.setTitleColor(UIColor.white, for: .normal)
        self.btnAbout.setTitleColor(UIColor.darkGray, for: .normal)
        
        if self.bannerImage != ""  {
            self.imgBanner.setImageFromUrl(urlStr: self.bannerImage)
        }
        
        self.tblView.dataSource = self
        self.tblView.delegate = self
        
        //Alter the animations as you want
        self.tblView.expandingAnimation = .fade
        self.tblView.collapsingAnimation = .fade
        
        print("Course Name: \(self.courseName)")
        self.GetInstituteDetail()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:InstituteDetailsHeaderView.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: InstituteDetailsHeaderView.self))
        
        let cell1 = UINib(nibName:String(describing:InstituteCourseCell.self), bundle: nil)
        self.tblView.register(cell1, forCellReuseIdentifier: String(describing: InstituteCourseCell.self))
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Institute Detail Service Calling
    
    func GetInstituteDetail() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id , "institute_id":self.instituteId ]
        print(params)
        
        HomeServices.GetInstituteDetail(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let description = response!["Result"]["description"].string {
                self.txtAbout.attributedText = description.html2AttributedString
            }
            
            if let fbLink = response!["Result"]["facebook_url"].string {
                self.btnFBLink.setTitle(fbLink, for: .normal)
            }
            
            let instituteResponseArray = response?["Result"]["courses"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  Course(json: resultObj)
                self.InstituteDetailArray?.append(obj)
            }
            
            let count = self.InstituteDetailArray!.count
            
            if count == 0 {
                
                self.emptyView.isHidden = false
                self.tblView.isHidden = true
                
            } else {
                
                self.emptyView.isHidden = true
                self.tblView.isHidden = false
                
            }
            
            self.getUniqueNames(UniqueName: self.InstituteDetailArray!)
            
        })
    }
    
    func getUniqueNames(UniqueName :[Course])->[String] {
        
        // Different Course Name
        let name = UniqueName.map { $0.name!}
        print("Course Name : \(name)")
        
        // Different Course Names in different Array
        let sameNameArrayGroup = Array(Dictionary(grouping:name){$0}.values)
        print("Same Course Name Array Groups \(sameNameArrayGroup)")
        
        for (index, item) in self.InstituteDetailArray!.enumerated() {
            self.data.append([item])
        }
        
        var names = [String]()
        
        for (idx, item) in self.data.enumerated() {
            for (index, j) in item.enumerated() {
                if !names.contains(j.name!) {
                    i.append([j])
                    names.append(j.name!)
                    i[i.count-1].append(j)
                } else {
                    let k = names.firstIndex(of: j.name!)
                    i[k!].append(j)
                }
            }
        }
        
        
        self.temp = self.data
        self.tblView.reloadData()
        
//
//        if self.courseName != "" {
//            let indexPath = names.firstIndex(of: self.courseName )
//            tblView.selectRow(at: indexPath, animated: false, scrollPosition: .bottom)
//        }
        
        return Array(name)
    }
    
    @IBAction func coursesAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.btnCourse.backgroundColor = UIColor.init(rgb: 0x78C395)
        self.btnAbout.backgroundColor = UIColor.white
        
        self.btnCourse.setTitleColor(UIColor.white, for: .normal)
        self.btnAbout.setTitleColor(UIColor.darkGray, for: .normal)
        
        self.aboutView.isHidden = true
        self.fbLinkView.isHidden = true
        self.imgBanner.isHidden = false
        self.tblView.isHidden = false
    }
    
    @IBAction func aboutAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        
        self.btnCourse.backgroundColor = UIColor.white
        self.btnAbout.backgroundColor = UIColor.init(rgb: 0x78C395)
        
        self.btnCourse.setTitleColor(UIColor.darkGray, for: .normal)
        self.btnAbout.setTitleColor(UIColor.white, for: .normal)
        
        self.imgBanner.isHidden = true
        self.tblView.isHidden = true
        self.fbLinkView.isHidden = false
        self.aboutView.isHidden = false
    }
    
    @IBAction func fbAction() {
        self.view.endEditing(true)
        
        let link = self.btnFBLink.titleLabel!.text!
        UIApplication.tryURL(urls: [link])
        
    }
}

extension InstituteDetailsViewController: ExpyTableViewDelegate , ExpyTableViewDataSource {
    
    func canExpand(section: Int, inTableView tableView: ExpyTableView) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int ) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InstituteDetailsHeaderView.self)) as! InstituteDetailsHeaderView
        
        if let name = i[section].first!.name {
            cell.lblTitle.text = name
        }
        
        return cell
    }
    
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
        
        switch state {
        case .willExpand:
            print("WILL EXPAND")
        case .willCollapse:
            print("WILL COLLAPSE")
            
        case .didExpand:
            print("DID EXPAND")
            
            if self.selectedInsitute != nil {
                tableView.selectRow(at: IndexPath(row: self.selectedInsitute + 1, section: 0), animated: false, scrollPosition: .none)
            }
            
        case .didCollapse:
            print("DID COLLAPSE")
            
        }
    }
}

extension InstituteDetailsViewController {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
        
        if indexPath.row == 0 {
            section = indexPath.section
            return
        }
        
        self.selectedInsitute = indexPath.row - 1
        
        if tableView.indexPathsForSelectedRows == nil {
            return
        }
        
        for index in tableView.indexPathsForSelectedRows! {
            if index.row != 0 && index != indexPath && index.section == indexPath.section {
                tableView.deselectRow(at: index, animated: false)
            }
        }
        self.selectedInsitute = nil
        
        let obj = (i[indexPath.section])[indexPath.row]
        
        let vc = CourseDetailViewController.instantiateFromStoryboard()
        vc.courseId = obj.id!
        vc.InstituteId = self.instituteId
        vc.courseObj = obj
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if section == indexPath.section {
            if indexPath.row == 0 {
                return GISTUtility.convertToRatio(70)
            }
            return GISTUtility.convertToRatio(140)
        }
        return GISTUtility.convertToRatio(70)
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.tblView.collapse(indexPath.section)
    }
    
}

//MARK: UITableView Data Source Methods

extension InstituteDetailsViewController {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return i.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("Row count for section \(section) is ")
        return i[section].count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: InstituteCourseCell.self)) as! InstituteCourseCell
        
        if let courseName = (i[indexPath.section])[indexPath.row].name {
            cell.lblCourseName.text = courseName
        }
        
        if let teacherName = (i[indexPath.section])[indexPath.row].teacher!.name {
            cell.lblTeacherName.text = teacherName
        }
        
        if let date = (i[indexPath.section])[indexPath.row].batches?.first?.startDate {
            cell.lblDate.text = "Starting from : \(date)"
        } else {
            cell.lblDate.text = "Starting from : N/A"
        }
        
        if let fee = (i[indexPath.section])[indexPath.row].fees {
            cell.lblPrice.text = "PKR\n\(fee)"
        }

        if self.selectedInsitute != nil && indexPath.row == self.selectedInsitute + 1 {
            cell.setSelected(true, animated: false)
        }
        
        return cell
        
    }
}



