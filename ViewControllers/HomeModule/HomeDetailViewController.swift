//
//  HomeDetailViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 15/06/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension HomeDetailViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    
}

class HomeDetailViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeDetailViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeDetailViewController
    }
    
    @IBOutlet weak var lblCourseName:UILabel!
    @IBOutlet weak var lblTeacherName:UILabel!
    @IBOutlet weak var lblStartDate:UILabel!
    @IBOutlet weak var lblEndDate:UILabel!
    @IBOutlet weak var lblFees:UILabel!
    @IBOutlet weak var lblTimeMonday:UILabel!
    @IBOutlet weak var lblTimeTuesday:UILabel!
    @IBOutlet weak var lblTimeWednesday:UILabel!
    @IBOutlet weak var lblTimeThursday:UILabel!
    @IBOutlet weak var lblTimeFriday:UILabel!
    @IBOutlet weak var lblTimeSaturday:UILabel!
    @IBOutlet weak var lblTimeSunday:UILabel!
    @IBOutlet weak var lblDescription:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var resultObj:Course!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = self.resultObj.name {
            
            let courseNum = self.resultObj.num
            
            if courseNum == 0 {
                self.title = title
            } else {
                self.title = title  + " \(courseNum!)"
            }
            
        }
        
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.initData()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initData() {
        
        if let courseName = self.resultObj.name {
            
            let courseNum = self.resultObj.num
            
            if courseNum == 0 {
                self.lblCourseName.text = courseName
            } else {
                self.lblCourseName.text = courseName  + " \(courseNum!)"
            }
            
        }
        
        if let name = self.resultObj.teacher!.name {
            self.lblTeacherName.text = name
        }
        
        if let startDate = self.resultObj.batches?.first!.startDate {
            self.lblStartDate.text = startDate
        }
        
        if let endDate = self.resultObj.batches?.first!.endDate {
            self.lblEndDate.text = endDate
        }
                
        if let days = self.resultObj.batches!.first!.batchdays {
            
            for batch in days {
                if batch.day == "Monday" {
                    self.lblTimeMonday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Tuesday" {
                    self.lblTimeTuesday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Wednesday" {
                    self.lblTimeWednesday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Thursday" {
                    self.lblTimeThursday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Friday" {
                    self.lblTimeFriday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Saturday" {
                    self.lblTimeSaturday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
                if batch.day == "Sunday" {
                    self.lblTimeSunday.text = "\(batch.toTime!) to \(batch.fromTime!)"
                }
            }
            
        }
        
        if let fees = self.resultObj.fees {
            self.lblFees.text = "PKR \(fees)"
        }
                
        if let description = self.resultObj.descriptionValue {
            self.lblDescription.text = description
        }
        
    }
    
}


