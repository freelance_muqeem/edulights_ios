//
//  TeacherProfileViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 15/06/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension TeacherProfileViewController : AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
    
}

class TeacherProfileViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> TeacherProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! TeacherProfileViewController
    }
    
    @IBOutlet weak var imgTeacher:UIImageView!
    @IBOutlet weak var lblTeacherName:UILabel!
    @IBOutlet weak var lblTeacherEducation:UILabel!
    @IBOutlet weak var lblTeacherEmail:UILabel!
    @IBOutlet weak var lblTeacherMobile:UILabel!
    @IBOutlet weak var lblTeacherDescription:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var resultObj:CourseDetail!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let title = self.resultObj.teacher!.name {
            self.title = title
        }
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        self.initData()
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func initData() {
        
        if let image = self.resultObj.teacher!.profileImage {
            self.imgTeacher.setImageFromUrl(urlStr: image)
        }
        
        if let name = self.resultObj.teacher!.name {
            self.lblTeacherName.text = name
        }
        
        if let education = self.resultObj.teacher!.qualification {
            self.lblTeacherEducation.text = education
        }
        
//        for a in self.resultObj.teacher!.courses! {
//            self.lblTeacherEducation.text = self.courseStrAppendWithComa(courseArray: [a])
//        }
        
        if let email = self.resultObj.teacher!.email {
            self.lblTeacherEmail.text = email
        }
        
        if let number = self.resultObj.teacher!.mobile {
            self.lblTeacherMobile.text = number
        }
        
        if let description = self.resultObj.teacher!.descriptionValue {
            self.lblTeacherDescription.text = description
        }
        
    }
    
    //MARK :- Array Name Comma Seperated
    
    func courseStrAppendWithComa(courseArray:[TeacherCourses]) -> String {
        
        var courseTitleArray:[String] = [String]()
        for obj in courseArray {
            courseTitleArray.append(obj.name!)
        }
        
        return courseTitleArray.joined(separator: ",")
        
    }
    
}

