//
//  HomeViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 25/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown

extension HomeViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

extension HomeViewController: EmailVerificationViewDelegate {
    
    func doneAction() {
        
        self.view.endEditing(true)
        
        let email = Singleton.sharedInstance.CurrentUser!.email!
        let token = self.emailVerificationView.txtToken.text!
        
        if token.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter verification email token", style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["email":email , "verify_token":token]
        print(params)
        
        HomeServices.EmailVerificationToken(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.showBanner(title: "Error", subTitle: msg! , style: .danger)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            let userResultObj = User(object:(response?["Result"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            self.emailVerificationView.isHidden = true
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Congratulations", msg: "Your email verification has been completed successfully", id: 0)
            self.alertView.isHidden = false
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: EMAILUPDATED), object: nil)
            UserDefaults.standard.set("true", forKey: "Email_Verified")
                        
        })
        
    }
    
}

extension HomeViewController {
    
    func updateDeviceId() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        var deviceId:String = ""
        let userId = Singleton.sharedInstance.CurrentUser!.id!
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
                
        if let token = UserDefaults.standard.string(forKey: "Token") {
            deviceId = token
        }
        
        let params:[String:Any] = ["device_token": deviceId , "user_id":userId , "device_type":"iOS"]
        print(params)
        
        HomeServices.UpdateToken(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            print("Response: \(response!)")
            
        })
        
    }
    
    func getBannerImage() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id]
        print(params)
        
        HomeServices.GetBannerImage(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            print("Response: \(response!)")
            
            if let image = response!["Result"]["advertise_image"].string {
                self.imgBanner.setImageFromUrl(urlStr: image )
                self.bannerImage = image
            }
            
        })
        
    }
    
}

extension HomeViewController : AlertViewDelegateAction {
    
    func okButtonAction() {
        
        UserDefaults.standard.removeObject(forKey: User_data_userDefault)
        UserDefaults.standard.removeObject(forKey: "Email_Verified")
        UserDefaults.standard.synchronize()
        Singleton.sharedInstance.CurrentUser = nil
        
        let nav = RootViewController.instantiateFromStoryboard()
        AppDelegate.getInstatnce().window?.rootViewController = nav
        let VC = LoginViewController.instantiateFromStoryboard()
        nav.pushViewController(VC, animated: true)
        
    }
    
    func cancelAction() {
        self.alertView.isHidden = true
    }
    
}

extension HomeViewController: UITextFieldDelegate {
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        self.searchTypeName.removeAll()
        
        if (textField.text?.isEmpty)! || textField.text == "" {
            self.searchTypeDropdown.hide()
        }
        else {
            
            self.searchTypeName.append( textField.text! + " in category " + "Course")
            self.searchTypeName.append( textField.text! + " in category " + "Institute")
            
            self.searchTypeDropdown.dataSource = self.searchTypeName
            self.searchTypeDropdown.show()
        }
    }
}

class HomeViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HomeViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HomeViewController
    }
    
    @IBOutlet weak var txtSearch:UITextField!
    
    @IBOutlet weak var pastLineView:UIView!
    @IBOutlet weak var currentLineView:UIView!
    @IBOutlet weak var upcomingLineView:UIView!
    
    @IBOutlet weak var btnPast:UIButton!
    @IBOutlet weak var btnCurrent:UIButton!
    @IBOutlet weak var btnUpcoming:UIButton!
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var emptyView:UIView!
    @IBOutlet weak var lblNoData:UILabel!
    
    @IBOutlet weak var imgBanner:UIImageView!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    @IBOutlet weak var emailVerificationView:EmailVerificationView!
    var textAlert:String?
    
    var searchTypeDropdown = DropDown()
    var courseListArray:[Course]? = [Course]()
    var courseCounts: [String: Int] = [:]
    var courseNameArray:[String] = []
    
    var isLogout:Bool = false
    var isEmail:Bool = false
    var height:Int? = 0
    
    var searchTypeName = [String]()
    var bannerImage:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Courses"
        self.navigationController?.ShowNavigationBar()
        self.PlaceNavigationButtons(selectorForLeft: #selector(menuAction) , leftImage: MENU_IMAGE , selectorForRight: #selector(notificationAction) , rightImage: NOTIFICATION_IMAGE )
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.txtSearch.delegate = self
        self.txtSearch.addTarget(self, action: #selector(self.textFieldDidChange(_:)), for: UIControl.Event.editingChanged)
        
        self.lblNoData.text = "No Course found \n kindly search to find a course"
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        self.alertView.delegateAction = self
        
        self.pastLineView.isHidden = true
        self.currentLineView.isHidden = false
        self.upcomingLineView.isHidden = true
        
        // Search Category DropDown
        
        self.searchTypeDropdown.anchorView = self.txtSearch // UIView or UIBarButtonItem
        self.searchTypeDropdown.bottomOffset = CGPoint(x: 0, y:(searchTypeDropdown.anchorView?.plainView.bounds.height)!)
        self.searchTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.view.endEditing(true)
            
            self.searchTypeName.removeAll()
            
            if index == 0 {
                
                let vc = InstituteViewController.instantiateFromStoryboard()
                vc.searchText = self.txtSearch.text!
                vc.bannerImage = self.bannerImage
                vc.typeId = 2
                self.navigationController?.pushViewController(vc, animated: true)
            }
            else {
                
                let vc = InstituteViewController.instantiateFromStoryboard()
                vc.searchText = self.txtSearch.text!
                vc.bannerImage = self.bannerImage
                vc.typeId = 1
                self.navigationController?.pushViewController(vc, animated: true)
            }
            
        }
        
        self.GetMyCourseList(type: "current")
        self.getBannerImage()
        
        if self.isLogout == true {
            
            DispatchQueue.main.async {
                
                self.alertView.btnOkAction.setTitle("Logout", for: .normal)
                self.alertView.btnCancel.setTitle("Cancel", for: .normal)
                
                self.alertView.alertShow(image: FAILURE_IMAGE , title: "Alert", msg: "Are you sure you want to Logout ?", id: 1)
                self.alertView.isHidden = false
                
            }
        }
        else {
            self.updateDeviceId()
        }
        
        if self.isEmail != false {
            
            let email = Singleton.sharedInstance.CurrentUser!.email!
            self.emailVerificationView.txtEmail.text = email
            self.emailVerificationView.delegate = self
            self.emailVerificationView.isHidden = false
            
        } else {
            
            self.emailVerificationView.delegate = self
            self.emailVerificationView.isHidden = true
        }
        
        //Register Cell
        let cell = UINib(nibName:String(describing:CourseCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: CourseCell.self))
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.txtSearch.text = ""
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func notificationAction() {
        let vc = NotificationViewController.instantiateFromStoryboard()
        vc.isOpenSideMenu = false
        let navVc = UINavigationController(rootViewController: vc)
        self.present(navVc, animated:true, completion: nil)
    }
    
    
    //MARK:- Get MyCourse List Service Calling
    
    func GetMyCourseList(type:String) {
        
        self.view.endEditing(true)
        self.courseListArray?.removeAll()
        self.courseCounts.removeAll()
        self.courseNameArray.removeAll()
        self.tblView.reloadData()
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id , "type":type]
        print(params)
        
        HomeServices.GetCourse(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.courseListArray?.removeAll()
            
            let propertyResponseArray = response?["Result"].arrayValue
            for resultObj in propertyResponseArray! {
                let obj = Course(json: resultObj)
                self.courseListArray?.append(obj)
            }
            
            let count = self.courseListArray!.count
            
            if count == 0 {
                
                self.emptyView.isHidden = false
                self.tblView.isHidden = true
                
            } else {
                
                self.emptyView.isHidden = true
                self.tblView.isHidden = false
                
            }
            
            self.getUniqueNames(UniqueName: self.courseListArray!)
            self.tblView.reloadData()
            
        })
    }
    
    func getUniqueNames(UniqueName :[Course])->[String] {
        
        // Different Merchant Name
        let name = UniqueName.map { $0.name!}
        print("Course Name : \(name)")
        
        // Different Merchant Names in different Array
        let sameNameArrayGroup = Array(Dictionary(grouping:name){$0}.values)
        print("Same Course Name Array Groups \(sameNameArrayGroup)")
        
        // Number of Different Merchant Names Array
        name.forEach { courseCounts[$0, default: 0] += 1 }
        print("Section Counts: \(courseCounts)")
        print("Course Names Sorted: \(courseCounts.keys)")
        
        return Array(name)
    }
    
    
    @IBAction func searchAction( _ sender : UIButton) {
        
        if self.txtSearch.text!.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Search can't be empty", style: .danger)
            return
        }
        
        let vc = InstituteViewController.instantiateFromStoryboard()
        vc.searchText = self.txtSearch.text!
        vc.bannerImage = self.bannerImage
        vc.typeId = 1
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func pastAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        self.pastLineView.isHidden = false
        self.currentLineView.isHidden = true
        self.upcomingLineView.isHidden = true
        
        self.GetMyCourseList(type: "past")
        
    }
    
    @IBAction func currentAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        self.pastLineView.isHidden = true
        self.currentLineView.isHidden = false
        self.upcomingLineView.isHidden = true
        
        self.GetMyCourseList(type: "current")
        
    }
    
    @IBAction func upcomingAction( _ sender : UIButton) {
        
        self.view.endEditing(true)
        self.pastLineView.isHidden = true
        self.currentLineView.isHidden = true
        self.upcomingLineView.isHidden = false
        
        self.GetMyCourseList(type: "upcoming")
        
    }
    
}

extension HomeViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.courseCounts.count
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        if courseCounts.count == 0 {
            return 0
        }
        
        self.height = 0
        let val = Array(courseCounts)[indexPath.row].value
        self.height = (val * 120) + 100
        
        print("Row height: \(self.height!)")
        return GISTUtility.convertToRatio(CGFloat(height!))
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let name = Array(courseCounts)[indexPath.row].key
        
        let cell:CourseCell = tableView.dequeueReusableCell(withIdentifier:String(describing:CourseCell.self)) as!  CourseCell
        
        let obj = self.courseListArray?.filter { (item) -> Bool in
            return Array(courseCounts)[indexPath.row].key == item.name!
        }
        
        cell.lblName.text = name
        
        if let obj = obj {
            cell.MyCourseListArray = obj
        }
        
        cell.navigationDelegate = self
        cell.collectionView.reloadData()
        cell.indexpath = indexPath
        
        return cell
    }
    
}

extension HomeViewController : NavigationDelegate {
    
    func didNavigate(course: Course) {
        
        let vc = HomeDetailViewController.instantiateFromStoryboard()
        vc.resultObj = course
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

extension Array where Element: Equatable {
    mutating func removeOBJ(_ obj: Element) {
        self = self.filter { $0 != obj }
    }
}




