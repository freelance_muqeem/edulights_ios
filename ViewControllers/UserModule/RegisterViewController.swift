//
//  RegisterViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 25/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown
import ALCameraViewController

extension RegisterViewController: AlertViewDelegate {
    
    func okAction() {
        
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2 ) {
            
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            AppDelegate.getInstatnce().window?.rootViewController = controller
            self.alertView.isHidden = true
            
        }

    }
    
}

class RegisterViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> RegisterViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! RegisterViewController
    }
    
    @IBOutlet weak var lblTitle:UILabel!
    
    @IBOutlet weak var btnOne:UIButton!
    @IBOutlet weak var btnTwo:UIButton!
    @IBOutlet weak var btnThree:UIButton!
    
    @IBOutlet weak var viewOne:UIView!
    @IBOutlet weak var viewTwo:UIView!
    @IBOutlet weak var viewThree:UIView!
    
    @IBOutlet weak var personalView:UIView!
    @IBOutlet weak var otherView:UIView!
    @IBOutlet weak var educationalView:UIView!
    
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtEmergencyPhone:UITextField!
    @IBOutlet weak var txtGuardianName:UITextField!
    @IBOutlet weak var txtGuardianPhone:UITextField!
    
    @IBOutlet weak var txtEducationType:UITextField!
    @IBOutlet weak var imgOne:UIImageView!
    @IBOutlet weak var imgTwo:UIImageView!
    @IBOutlet weak var imgThree:UIImageView!
    @IBOutlet weak var imgFour:UIImageView!
    @IBOutlet weak var imgFive:UIImageView!
    
    @IBOutlet weak var imgOneCross:UIImageView!
    @IBOutlet weak var imgTwoCross:UIImageView!
    @IBOutlet weak var imgThreeCross:UIImageView!
    @IBOutlet weak var imgFourCross:UIImageView!
    @IBOutlet weak var imgFiveCross:UIImageView!
    
    @IBOutlet weak var btnOneCross:UIButton!
    @IBOutlet weak var btnTwoCross:UIButton!
    @IBOutlet weak var btnThreeCross:UIButton!
    @IBOutlet weak var btnFourCross:UIButton!
    @IBOutlet weak var btnFiveCross:UIButton!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    
    var isImageOne:Bool! = false
    var isImageTwo:Bool! = false
    var isImageThree:Bool! = false
    var isImageFour:Bool! = false
    var isImageFive:Bool! = false
    var imageArray:[UIImage] = []

    var educationTypeDropdown = DropDown()
    var educationTypeArray:[EducationList]? = [EducationList]()
    var educationTypeID:Int! = 0
    
    var userResultObj: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Registration"
        self.navigationController?.ShowNavigationBar()
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.alertView.delegate = self
        self.alertView.isHidden = true
        
        // Education List DropDown
        educationTypeDropdown.anchorView = self.txtEducationType // UIView or UIBarButtonItem
        educationTypeDropdown.bottomOffset = CGPoint(x: 0 , y: (educationTypeDropdown.anchorView?.plainView.bounds.height)!)
        educationTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.txtEducationType.text = item
            
            for a in self.educationTypeArray! {
                
                if item == a.title {
                    self.educationTypeID = a.id!
                    print("Education Id: \(self.educationTypeID!)")
                }
            }
            
        }
        
        self.lblTitle.text = "CONTACT INFORMATION"
        self.personalView.isHidden = false
        self.otherView.isHidden = true
        self.educationalView.isHidden = true
        
        self.btnOne.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnTwo.setTitleColor(UIColor(rgb: 0x6EB389), for: .normal)
        self.btnThree.setTitleColor(UIColor(rgb: 0x6EB389), for: .normal)
        self.btnThree.isUserInteractionEnabled = false
        
        self.viewOne.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewTwo.backgroundColor = UIColor(rgb: 0xFFFFFF)
        self.viewThree.backgroundColor = UIColor(rgb: 0xFFFFFF)
        
        self.imgOneCross.isHidden = true
        self.imgTwoCross.isHidden = true
        self.imgThreeCross.isHidden = true
        self.imgFourCross.isHidden = true
        self.imgFiveCross.isHidden = true
        
        self.btnOneCross.isHidden = true
        self.btnTwoCross.isHidden = true
        self.btnThreeCross.isHidden = true
        self.btnFourCross.isHidden = true
        self.btnFiveCross.isHidden = true
        
        self.GetEducationList()
        
    }
    
    @objc func backAction() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func firstAction(_ sender : UIButton) {
        
        self.lblTitle.text = "CONTACT INFORMATION"
        self.personalView.isHidden = false
        self.otherView.isHidden = true
        self.educationalView.isHidden = true
        
        self.btnOne.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnTwo.setTitleColor(UIColor(rgb: 0x6EB389), for: .normal)
        self.btnThree.setTitleColor(UIColor(rgb: 0x6EB389), for: .normal)
        self.btnThree.isUserInteractionEnabled = false

        self.viewOne.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewTwo.backgroundColor = UIColor(rgb: 0xFFFFFF)
        self.viewThree.backgroundColor = UIColor(rgb: 0xFFFFFF)
        
    }
    
    @IBAction func secondAction(_ sender : UIButton) {
        self.checkPersonalData()
    }
    
    @IBAction func thirdAction(_ sender : UIButton) {
        self.checkOtherData()
    }
    
    @IBAction func continuePersonalAction(_ sender : UIButton) {
        self.checkPersonalData()
    }
    
    @IBAction func continueOtherAction(_ sender : UIButton) {
        self.checkOtherData()
    }
    
    func checkPersonalData() {
        
        let firstName = self.txtFirstName.text!
        let lastName = self.txtLastName.text!
        let password = self.txtPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if firstName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter first name" , style: .danger)
            return
        }
        
        if lastName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter last name" , style: .danger)
            return
        }
        
        guard let number = self.txtPhone.text, AppHelper.isValidPhone(testStr: number) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid phone number" , style: .danger)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        if  password.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter password" , style: .danger)
            return
        }
        
        if  confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter confirm password" , style: .danger)
            return
        }
        
        guard let validNewPassword = self.txtPassword.text , AppHelper.isValidPassword(testStr: validNewPassword) else {
            self.showBanner(title: "Error", subTitle: "Please enter minimum 6 digit password" , style: .danger)
            return
        }
        
        if validNewPassword != confirmPassword {
            self.showBanner(title: "Error", subTitle: "Password & confirm password does not match" , style: .danger)
            return
        }
        
        self.lblTitle.text = "OTHER INFORMATION"
        self.personalView.isHidden = true
        self.otherView.isHidden = false
        self.educationalView.isHidden = true
        
        self.btnOne.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnTwo.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnThree.setTitleColor(UIColor(rgb: 0x6EB389), for: .normal)
        self.btnThree.isUserInteractionEnabled = true
        
        self.viewOne.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewTwo.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewThree.backgroundColor = UIColor(rgb: 0xFFFFFF)
        
    }
    
    func checkOtherData() {
        
        let address = self.txtAddress.text!
        let city = self.txtCity.text!
        let guardianName = self.txtGuardianName.text!
        
        if address.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter address" , style: .danger)
            return
        }
        
        if city.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter city" , style: .danger)
            return
        }
        
        guard let emergencyPhone = self.txtEmergencyPhone.text, AppHelper.isValidPhone(testStr: emergencyPhone) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid emergency phone number" , style: .danger)
            return
        }
        
        if guardianName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter guardian name" , style: .danger)
            return
        }
        
        guard let guardianPhone = self.txtGuardianPhone.text, AppHelper.isValidPhone(testStr: guardianPhone) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid guardian phone number" , style: .danger)
            return
        }
        
        self.lblTitle.text = "EDUCATIONAL INFORMATION"
        self.personalView.isHidden = true
        self.otherView.isHidden = true
        self.educationalView.isHidden = false
        
        self.btnOne.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnTwo.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        self.btnThree.setTitleColor(UIColor(rgb: 0xFFFFFF), for: .normal)
        
        self.viewOne.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewTwo.backgroundColor = UIColor(rgb: 0x6EB389)
        self.viewThree.backgroundColor = UIColor(rgb: 0x6EB389)
        
    }
    
    @IBAction func documentTypeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.educationTypeDropdown.show()
    }
    
    //MARK:-  Education Type Listing Service Calling
    
    func GetEducationList() {
        
        self.view.endEditing(true)
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.EducationList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            if let educationResponseArray = response?["Result"].array {
                
                var educationItem = [String]()
                
                for resultObj in educationResponseArray {
                    let obj =  EducationList(json: resultObj)
                    self.educationTypeArray?.append(obj)
                    if let title = obj.title {
                        educationItem.append((title))
                    }
                }
                
                self.educationTypeDropdown.dataSource = educationItem
                
            }
            
        })
        
    }

    
    
    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let firstName = self.txtFirstName.text!
        let lastName = self.txtLastName.text!
        let phoneNumber = self.txtPhone.text!
        let email = self.txtEmail.text!
        let password = self.txtPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        let address = self.txtAddress.text!
        let city = self.txtCity.text!
        let emergencyPhoneNumber = self.txtEmergencyPhone.text!
        let guardianName = self.txtGuardianName.text!
        let guardianPhoneNumber = self.txtGuardianPhone.text!
        let educationType = self.txtEducationType.text!
        
        if educationType.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Kindly select education type" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        if self.isImageOne == true {
            self.imageArray.append(self.imgOne.image!)
        }
        if self.isImageTwo == true {
            self.imageArray.append(self.imgTwo.image!)
        }
        if self.isImageThree == true {
            self.imageArray.append(self.imgThree.image!)
        }
        if self.isImageFour == true {
            self.imageArray.append(self.imgFour.image!)
        }
        if self.isImageFive == true {
            self.imageArray.append(self.imgFive.image!)
        }
        
        let params:[String:Any] = ["email":email , "first_name":firstName , "last_name":lastName , "phone":phoneNumber , "address":address , "city":city , "emergency_contact":emergencyPhoneNumber , "guardian_name":guardianName , "guardian_phone":guardianPhoneNumber , "password":password , "password_confirmation":confirmPassword , "education_type":educationType]
        print("Parameter: \(params)")
        print("Image Count: \(self.imageArray.count)")
        
        UserServices.Register(param: params, images: imageArray, completionHandler:{(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            self.userResultObj = User(object:(response?["Result"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: self.userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Congratulations", msg: "Your account has been created successfully", id: 0)
            self.alertView.isHidden = false
            
        })

    }
}

extension RegisterViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func imageOneAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 1)
    }
    
    @IBAction func imageTwoAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 2)
    }
    
    @IBAction func imageThreeAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 3)
    }
    
    @IBAction func imageFourAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 4)
    }
    
    @IBAction func imageFiveAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 5)
    }
    
    func openCamera(id:Int){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let pickedImage = image {
                
                if id == 1 {
                    
                    self?.isImageOne = true
                    self?.imgOne.image = pickedImage
                    
                    self?.imgOneCross.isHidden = false
                    self?.btnOneCross.isHidden = false
                    
                }
                else if id == 2 {
                    
                    self?.isImageTwo = true
                    self?.imgTwo.image = pickedImage
                    
                    self?.imgTwoCross.isHidden = false
                    self?.btnTwoCross.isHidden = false
                    
                }
                else if id == 3 {
                    
                    self?.isImageThree = true
                    self?.imgThree.image = pickedImage
                    
                    self?.imgThreeCross.isHidden = false
                    self?.btnThreeCross.isHidden = false
                    
                }
                else if id == 4 {
                    
                    self?.isImageFour = true
                    self?.imgFour.image = pickedImage
                    
                    self?.imgFourCross.isHidden = false
                    self?.btnFourCross.isHidden = false
                    
                }
                else if id == 5 {
                    
                    self?.isImageFive = true
                    self?.imgFive.image = pickedImage
                    
                    self?.imgFiveCross.isHidden = false
                    self?.btnFiveCross.isHidden = false
                }
                
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
    @IBAction func CrossOneAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageOne = false
        self.imgOne.image = UIImage(named: "docs_icon")
        
        self.imgOneCross.isHidden = true
        self.btnOneCross.isHidden = true
    }
    
    @IBAction func CrosTwoAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageTwo = false
        self.imgTwo.image = UIImage(named: "docs_icon")
        
        self.imgTwoCross.isHidden = true
        self.btnTwoCross.isHidden = true
        
    }
    
    @IBAction func CrosThreeAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageThree = false
        self.imgThree.image = UIImage(named: "docs_icon")
        
        self.imgThreeCross.isHidden = true
        self.btnThreeCross.isHidden = true
        
    }
    
    @IBAction func CrosFourAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageFour = false
        self.imgFour.image = UIImage(named: "docs_icon")
        
        self.imgFourCross.isHidden = true
        self.btnFourCross.isHidden = true
        
    }
    
    @IBAction func CrosFiveAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageFive = false
        self.imgFive.image = UIImage(named: "docs_icon")
        
        self.imgFiveCross.isHidden = true
        self.btnFiveCross.isHidden = true
    }
    
}

