//
//  SettingsViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension SettingsViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class SettingsViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> SettingsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! SettingsViewController
    }
    
    @IBOutlet weak var notificationToggle:UISwitch!

    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Settings"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        if Singleton.sharedInstance.CurrentUser!.notificationStatus == 1 {
            self.notificationToggle.isOn = true
        } else {
            self.notificationToggle.isOn = false
        }
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    //MARK:- Notification Switch Action
    
    @IBAction func notificationStatusChange(_ sender: UISwitch ) {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let userId = Singleton.sharedInstance.CurrentUser!.id!
        var notificationValue:Int = 1
        
        if self.notificationToggle.isOn == true {
            notificationValue = 1
        } else {
            notificationValue = 0
        }
        
        let params:[String:Any] = ["user_id":userId , "notification_status":notificationValue]
        print(params)
        
        SettingServices.ChangePushStatus(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let userResultObj = User(object:(response?["Result"])!)
            UserDefaults.standard.removeObject(forKey: User_data_userDefault)
            UserDefaults.standard.synchronize()
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = userResultObj
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: "Your notification status has been changed successfully", id: 0)
            self.alertView.isHidden = false
            
        })

        
    }
    
    @IBAction func changePasswordAction(_ sender : UIButton) {
        let vc = ChangePasswordViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func contactUsAction(_ sender : UIButton) {
        let vc = ContactUsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func AboutUsAction(_ sender : UIButton) {
        let vc = AboutUsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func HelpAction(_ sender : UIButton) {
        let vc = HelpViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func TermsAction(_ sender : UIButton) {
        let vc = TermsConditionsViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacyAction(_ sender : UIButton) {
        let vc = PrivacyPolicyViewController.instantiateFromStoryboard()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

