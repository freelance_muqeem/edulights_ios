//
//  HelpViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension HelpViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class HelpViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> HelpViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! HelpViewController
    }
    
    @IBOutlet weak var txtHelp:UITextView!

    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Help"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.GetHelp()
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  About Service Calling
    
    func GetHelp() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["":""]
        print(params)
        
        SettingServices.GetHelp(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let text = response?["Result"]["content"].string {
                self.txtHelp.text = text
            }
            
        })
        
    }
    
}
