//
//  PrivacyPolicyViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension PrivacyPolicyViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class PrivacyPolicyViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> PrivacyPolicyViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PrivacyPolicyViewController
    }
    
    @IBOutlet weak var txtPrivacy:UITextView!

    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Privacy Policy"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.GetPrivacy()
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:-  Privacy Service Calling
    
    func GetPrivacy() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["":""]
        print(params)
        
        SettingServices.GetPrivacy(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let text = response?["Result"]["content"].string {
                self.txtPrivacy.attributedText = text.html2AttributedString
            }
            
        })
        
    }
    
}
