//
//  ChangePasswordViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ChangePasswordViewController: AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "Success" {
            self.navigationController?.popViewController(animated: true)
        }
        self.alertView.isHidden = true
    }
    
}

class ChangePasswordViewController: UIViewController {

    class func instantiateFromStoryboard() -> ChangePasswordViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ChangePasswordViewController
    }
    
    @IBOutlet weak var txtOldPassword:UITextField!
    @IBOutlet weak var txtNewPassword:UITextField!
    @IBOutlet weak var txtConfirmPassword:UITextField!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Change Password"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func changePasswordAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let userId = Singleton.sharedInstance.CurrentUser!.id!
        let oldPassword = self.txtOldPassword.text!
        let newPassword = self.txtNewPassword.text!
        let confirmPassword = self.txtConfirmPassword.text!
        
        if oldPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter old password" , style: .danger)
            return
        }
        
        if  newPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter new password" , style: .danger)
            return
        }
        
        if  confirmPassword.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter confirm password" , style: .danger)
            return
        }
        
        if  oldPassword.count < 6 || newPassword.count < 6  || confirmPassword.count < 6 {
            self.showBanner(title: "Alert", subTitle: "Please enter 6 digit password" , style: .danger)
            return
            
        }
        
        if newPassword != confirmPassword {
            self.showBanner(title: "Alert", subTitle: "Password & confirm password does not match" , style: .danger)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["user_id":userId , "old_password":oldPassword , "password":newPassword , "password_confirmation":confirmPassword ]
        print(params)
        
        SettingServices.ChangePassword(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: "Your Password has been changed successfully", id: 0)
            self.alertView.isHidden = false
            self.textAlert = "Success"
            
        })
        
    }

}
