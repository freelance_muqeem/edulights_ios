//
//  ContactUsViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 19/05/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension ContactUsViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class ContactUsViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> ContactUsViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! ContactUsViewController
    }
    
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtDescription:UITextView!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Contact Us"
        self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        if let name = Singleton.sharedInstance.CurrentUser!.firstName {
            if let lastName = Singleton.sharedInstance.CurrentUser!.lastName {
                self.txtName.text = name + " " + lastName
            }
        }

        if let email = Singleton.sharedInstance.CurrentUser!.email {
            self.txtEmail.text = email
        }
        
        if let phone = Singleton.sharedInstance.CurrentUser!.phone {
            self.txtPhone.text = phone
        }
        
        self.txtDescription.placeholder = "Your text here..."
        
    }
    
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let userId = Singleton.sharedInstance.CurrentUser!.id!
        let name = self.txtName.text!
        let description = self.txtDescription.text!
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter name" , style: .danger)
            return
        }
        
        guard let email = self.txtEmail.text, AppHelper.isValidEmail(testStr: email) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid email" , style: .danger)
            return
        }
        
        guard let phone = self.txtPhone.text, AppHelper.isValidPhone(testStr: phone) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid phone number" , style: .danger)
            return
        }
        
        if description.isEmptyOrWhitespace() {
            self.showBanner(title: "Alert", subTitle: "Please enter description" , style: .danger)
            return
            
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let params:[String:Any] = ["user_id":userId , "name":name , "email":email , "phone":phone , "description":description]
        print(params)
        
        SettingServices.ContactUs(param: params, completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Success", msg: "Your form has been submitted successfully", id: 0)
            self.alertView.isHidden = false
            
        })
        
    }

}

