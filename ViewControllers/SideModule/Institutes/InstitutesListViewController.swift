//
//  MessagesListViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension InstitutesListViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class InstitutesListViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> InstitutesListViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! InstitutesListViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    
    @IBOutlet weak var txtSearch:UITextField!
    @IBOutlet weak var lblIntro:UILabel!
    @IBOutlet weak var lblNoData:UILabel!
    
    var InstituteListArray:[InstituteData]? = [InstituteData]()
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Institute"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.lblIntro.isHidden = true
        self.lblNoData.isHidden = true
        
        self.GetInstituteList()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:InstituteCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: InstituteCell.self))
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func searchAction(_ sender : UIButton) {
        
        if self.txtSearch.text!.isEmptyOrWhitespace() {
            self.InstituteListArray?.removeAll()
            self.tblView.reloadData()
            self.GetInstituteList()
            return
        }
        
        self.InstituteListArray?.removeAll()
        self.tblView.reloadData()
        
        self.view.endEditing(true)
        self.GetSearchInstituteList()
        
    }
    
    //MARK:-  Institute Listing Service Calling
    
    func GetInstituteList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id]
        print(params)
        
        HomeServices.InstituteList(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let instituteResponseArray = response?["Result"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  InstituteData(json: resultObj)
                self.InstituteListArray?.append(obj)
            }
            
            if self.InstituteListArray!.count == 0 {
                
                self.lblIntro.isHidden = true
                self.tblView.isHidden = true
                self.lblNoData.isHidden = false
                
            } else {
                
                self.lblNoData.isHidden = true
                self.tblView.isHidden = false
                self.tblView.reloadData()
            }
            
        })
        
    }
    
    //MARK:- Search Institute Listing Service Calling
    
    func GetSearchInstituteList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let keyword = self.txtSearch.text!
        
        let params:[String:Any] = ["user_id":id , "keyword":keyword , "code":1]
        print(params)
        
        HomeServices.GetInstitute(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            if let count = response?["Result"]["count"].int {
                
                if count == 0 {
                    self.lblIntro.isHidden = true
                    self.tblView.isHidden = true
                    self.lblNoData.isHidden = false
                    return
                }
                
                
                self.lblIntro.text = "\(count) filter results for \(keyword)"
                
                self.lblIntro.isHidden = false
                self.tblView.isHidden = false
                self.lblNoData.isHidden = true
                
            }
            
            let instituteResponseArray = response?["Result"]["data"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  InstituteData(json: resultObj)
                self.InstituteListArray?.append(obj)
            }
            
            self.tblView.reloadData()
            
        })
        
    }
    
}

extension InstitutesListViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.InstituteListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(130)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:InstituteData = self.InstituteListArray![indexPath.row]
        
        let cell:InstituteCell = tableView.dequeueReusableCell(withIdentifier:String(describing:InstituteCell.self)) as!  InstituteCell
        
        if let image = obj.logoImage {
            cell.imgLogo.setImageFromUrl(urlStr: image)
        }
        
        if let name = obj.name {
            cell.lblName.text = name
        }
        
        if let address = obj.address {
            cell.lblAddress.text = address
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj:InstituteData = self.InstituteListArray![indexPath.row]
        
        let vc = InstituteDetailsViewController.instantiateFromStoryboard()
        vc.instituteId = obj.id!
        vc.instituteName = obj.name!
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}

