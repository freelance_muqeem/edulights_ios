//
//  PaymentViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0

extension PaymentViewController: AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "Success" {
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            AppDelegate.getInstatnce().window!.rootViewController = controller
        }
        
        self.alertView.isHidden = true
    }
    
}

class PaymentViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> PaymentViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! PaymentViewController
    }
    
    @IBOutlet weak var txtCardNumber:UITextField!
    @IBOutlet weak var txtExpiryDate:UITextField!
    @IBOutlet weak var txtCVVNumber:UITextField!
    @IBOutlet weak var txtFullName:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Payment Information"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        if let firstname = Singleton.sharedInstance.CurrentUser!.firstName {
            if let lastName = Singleton.sharedInstance.CurrentUser!.lastName {
                self.txtFullName.text = firstname + " " + lastName
            } else {
                self.txtFullName.text = firstname
            }
        }
        
        if let address = Singleton.sharedInstance.CurrentUser!.address {
            self.txtAddress.text = address
        }
        
        if let city = Singleton.sharedInstance.CurrentUser!.city {
            self.txtCity.text = city
        }
    }
    
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @IBAction func submitAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let cardNumber = self.txtCardNumber.text!
        let expiryDate = self.txtExpiryDate.text!
        let cvv = self.txtCVVNumber.text!
        let name = self.txtFullName.text!
        let address = self.txtAddress.text!
        let city = self.txtCity.text!
        
        if cardNumber.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter card number" , style: .danger)
            return
        }
        
        if expiryDate.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please select expiry date" , style: .danger)
            return
        }
        
        if name.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter full name" , style: .danger)
            return
        }
        
        if address.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter address" , style: .danger)
            return
        }
        
        if city.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter city" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = [ "user_id":id , "card_number":cardNumber , "expiry_date":expiryDate , "cvv":cvv , "billing_person_name":name , "billing_person_address":address , "billing_person_city":city]
        print("Parameter: \(params)")
        
        SettingServices.UpdatePayment(param: params, completionHandler:{(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Congratulations", msg: "Your payment information has been updated successfully", id: 0)
            self.alertView.isHidden = false
            self.textAlert = "Success"
            
        })
        
    }
    
    @IBAction func dateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        ActionSheetDatePicker.show(withTitle: "Expiry Date", datePickerMode: .date , selectedDate: Date(), minimumDate: Date() , maximumDate: nil, doneBlock: { (picker, date, origin) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd" //Your New Date format as per requirement change it own
            let newDate = dateFormatter.string(from: date! as! Date) //pass Date here
            self.txtExpiryDate.text = newDate
            
        }, cancel: { ActionMultipleStringCancelBlock in return }, origin: sender)
        
    }
    
}



