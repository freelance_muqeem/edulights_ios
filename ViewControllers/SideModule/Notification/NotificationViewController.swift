//
//  NotificationViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit

extension NotificationViewController: AlertViewDelegate {
    
    func okAction() {
        self.alertView.isHidden = true
    }
    
}

class NotificationViewController: UIViewController {

    class func instantiateFromStoryboard() -> NotificationViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! NotificationViewController
    }
    
    @IBOutlet weak var tblView:UITableView!
    @IBOutlet weak var lblNoData:UILabel!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    var isOpenSideMenu:Bool = false
    
    var NotificationListArray:[NotificationData]? = [NotificationData]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Notifications"
        if self.isOpenSideMenu == false {
            self.PlaceLeftButton(image: BACK_IMAGE , selector: #selector(backAction))
        } else {
            self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        }
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        self.GetNotificationList()
        
        //Register Cell
        let cell = UINib(nibName:String(describing:NotificationCell.self), bundle: nil)
        self.tblView.register(cell, forCellReuseIdentifier: String(describing: NotificationCell.self))

    }

    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    @objc func backAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:-  Notification Listing Service Calling
    
    func GetNotificationList() {
        
        self.view.endEditing(true)
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = ["user_id":id]
        print(params)
        
        HomeServices.GetNotification(param:params , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print(response!)
            
            let instituteResponseArray = response?["Result"].arrayValue
            
            for resultObj in instituteResponseArray! {
                let obj =  NotificationData(json: resultObj)
                self.NotificationListArray?.append(obj)
            }
            
            if self.NotificationListArray!.count == 0 {
                    self.tblView.isHidden = true
                    self.lblNoData.isHidden = false
            }
            else {
                
                self.tblView.isHidden = false
                self.lblNoData.isHidden = true
                self.tblView.reloadData()
                
            }
            
        })
        
    }
    
}


extension NotificationViewController : UITableViewDelegate , UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NotificationListArray!.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return GISTUtility.convertToRatio(85)
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let obj:NotificationData = self.NotificationListArray![indexPath.row]
        
        let cell:NotificationCell = tableView.dequeueReusableCell(withIdentifier:String(describing:NotificationCell.self)) as!  NotificationCell
        
        if let title = obj.actionType {
            cell.lblTitle.text = title
        } else {
            cell.lblTitle.text = "Notification"
        }
        
        if let message = obj.message {
            cell.lblDescription.text = message
        }

        if let date = obj.createdAt {
            cell.lblDate.text = date.GetTime(fromFormat: "yyyy-MM-dd HH:mm:ss", toFormat: "dd/MM/yyyy")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj:NotificationData = self.NotificationListArray![indexPath.row]

        self.alertView.alertShow(image: SUCCESS_IMAGE , title: "Notification", msg: obj.message! , id: 0)
        self.alertView.isHidden = false
        
    }
    
}


