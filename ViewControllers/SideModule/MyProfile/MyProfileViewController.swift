//
//  HelpViewController.swift
//  EduLights
//
//  Created by Abdul Muqeem on 30/04/2019.
//  Copyright © 2019 Abdul Muqeem. All rights reserved.
//

import UIKit
import DropDown
import Alamofire
import SwiftyJSON
import ALCameraViewController

extension MyProfileViewController: AlertViewDelegate {
    
    func okAction() {
        
        if self.textAlert == "Update Profile" {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: USERUPDATED), object: nil)
            let controller = SideMenuRootViewController.instantiateFromStoryboard()
            controller.leftViewPresentationStyle = .slideAbove
            AppDelegate.getInstatnce().window!.rootViewController = controller
        }
        
        self.alertView.isHidden = true
        
    }
    
}

class MyProfileViewController: UIViewController {
    
    class func instantiateFromStoryboard() -> MyProfileViewController {
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: String(describing: self)) as! MyProfileViewController
    }
    
    @IBOutlet weak var imgBackground:UIImageView!
    @IBOutlet weak var imgProfile:UIImageView!
    
    @IBOutlet weak var txtFirstName:UITextField!
    @IBOutlet weak var txtLastName:UITextField!
    @IBOutlet weak var txtPhone:UITextField!
    @IBOutlet weak var txtEmail:UITextField!
    @IBOutlet weak var txtAddress:UITextField!
    @IBOutlet weak var txtCity:UITextField!
    @IBOutlet weak var txtEmergencyPhone:UITextField!
    @IBOutlet weak var txtGuardianName:UITextField!
    @IBOutlet weak var txtGuardianPhone:UITextField!
    @IBOutlet weak var txtEducationType:UITextField!
    
    @IBOutlet weak var imgOne:UIImageView!
    @IBOutlet weak var imgTwo:UIImageView!
    @IBOutlet weak var imgThree:UIImageView!
    @IBOutlet weak var imgFour:UIImageView!
    @IBOutlet weak var imgFive:UIImageView!
    
    @IBOutlet weak var imgOneCross:UIImageView!
    @IBOutlet weak var imgTwoCross:UIImageView!
    @IBOutlet weak var imgThreeCross:UIImageView!
    @IBOutlet weak var imgFourCross:UIImageView!
    @IBOutlet weak var imgFiveCross:UIImageView!
    
    @IBOutlet weak var btnOneCross:UIButton!
    @IBOutlet weak var btnTwoCross:UIButton!
    @IBOutlet weak var btnThreeCross:UIButton!
    @IBOutlet weak var btnFourCross:UIButton!
    @IBOutlet weak var btnFiveCross:UIButton!
    
    // Alert
    @IBOutlet weak var alertView:AlertView!
    var textAlert:String?
    
    var isImageOne:Bool! = false
    var isImageTwo:Bool! = false
    var isImageThree:Bool! = false
    var isImageFour:Bool! = false
    var isImageFive:Bool! = false
    var imageArray:[UIImage] = []
    
    var educationTypeDropdown = DropDown()
    var educationTypeArray:[EducationList]? = [EducationList]()
    var educationTypeID:Int! = 0
    var images:[DocumentImages] = [DocumentImages]()
    
    var profileImage:String! = ""
    var isProfileImage:Bool! = false
    var deleted_document_IDs:String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "My Profile"
        self.PlaceLeftButton(image: MENU_IMAGE , selector: #selector(menuAction))
        self.navigationController?.ThemedNavigationBar()
        self.navigationController?.ChangeTitleColor(color: .white)
        self.navigationController?.ChangeTitleFont()
        
        self.alertView.isHidden = true
        self.alertView.delegate = self
        
        // Education List DropDown
        educationTypeDropdown.anchorView = self.txtEducationType // UIView or UIBarButtonItem
        educationTypeDropdown.bottomOffset = CGPoint(x: 0 , y: (educationTypeDropdown.anchorView?.plainView.bounds.height)!)
        educationTypeDropdown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            self.txtEducationType.text = item
            
            for a in self.educationTypeArray! {
                
                if item == a.title {
                    self.educationTypeID = a.id!
                    print("Education Id: \(self.educationTypeID!)")
                }
            }
            
        }
        
        self.imgOneCross.isHidden = true
        self.imgTwoCross.isHidden = true
        self.imgThreeCross.isHidden = true
        self.imgFourCross.isHidden = true
        self.imgFiveCross.isHidden = true
        
        self.btnOneCross.isHidden = true
        self.btnTwoCross.isHidden = true
        self.btnThreeCross.isHidden = true
        self.btnFourCross.isHidden = true
        self.btnFiveCross.isHidden = true
        
        self.initData()
        self.GetEducationList()
        
    }
    
    @objc func menuAction() {
        self.showLeftViewAnimated(self)
    }
    
    func initData() {
        
        if let image = Singleton.sharedInstance.CurrentUser!.profileImage {
            self.imgProfile.setImageFromUrl(urlStr: image)
            self.imgBackground.setImageFromUrl(urlStr: image)
        }
        
        if let firstName = Singleton.sharedInstance.CurrentUser!.firstName {
            self.txtFirstName.text = firstName
        }
        
        if let lastName = Singleton.sharedInstance.CurrentUser!.lastName {
            self.txtLastName.text = lastName
        }
        
        if let phone = Singleton.sharedInstance.CurrentUser!.phone {
            self.txtPhone.text = phone
        }
        
        if let email = Singleton.sharedInstance.CurrentUser!.email {
            self.txtEmail.text = email
        }
        
        if let address = Singleton.sharedInstance.CurrentUser!.address {
            self.txtAddress.text = address
        }
        
        if let city = Singleton.sharedInstance.CurrentUser!.city {
            self.txtCity.text = city
        }
        
        if let emergencyContact = Singleton.sharedInstance.CurrentUser!.emergencyContact {
            self.txtEmergencyPhone.text = emergencyContact
        }
        
        if let guardianName = Singleton.sharedInstance.CurrentUser!.guardianName {
            self.txtGuardianName.text = guardianName
        }
        
        if let guardianPhone = Singleton.sharedInstance.CurrentUser!.guardianPhone {
            self.txtGuardianPhone.text = guardianPhone
        }
        
        if let educationType = Singleton.sharedInstance.CurrentUser!.educationType {
            self.txtEducationType.text = educationType
        }
        
        if let images = Singleton.sharedInstance.CurrentUser!.documentImages {
            
            self.images = images
            let baseImageURL = "https://edulights.com/edulights/public/images/document_images/"
            
            if self.images.indices.contains(0) {
                if let firstImage = self.images[0].document {
                    self.imgOne.setImageFromUrl(urlStr: baseImageURL + firstImage)
                    self.imgOneCross.isHidden = false
                    self.btnOneCross.isHidden = false
                    self.isImageOne = true
                    
                }
            }
            
            if self.images.indices.contains(1) {
                if let secondImage = self.images[1].document {
                    self.imgTwo.setImageFromUrl(urlStr: baseImageURL + secondImage)
                    self.imgTwoCross.isHidden = false
                    self.btnTwoCross.isHidden = false
                    self.isImageTwo = true
                }
            }
            
            if self.images.indices.contains(2) {
                if let thirdImage = self.images[2].document {
                    self.imgThree.setImageFromUrl(urlStr: baseImageURL + thirdImage)
                    self.imgThreeCross.isHidden = false
                    self.btnThreeCross.isHidden = false
                    self.isImageThree = true
                    
                }
            }
            
            if self.images.indices.contains(3) {
                if let fourthImage = self.images[3].document {
                    self.imgFour.setImageFromUrl(urlStr: baseImageURL + fourthImage)
                    self.imgFourCross.isHidden = false
                    self.btnFourCross.isHidden = false
                    self.isImageFour = true
                    
                }
            }
            
            if self.images.indices.contains(4) {
                if let fifthImage = self.images[4].document {
                    self.imgFive.setImageFromUrl(urlStr: baseImageURL + fifthImage)
                    self.imgFiveCross.isHidden = false
                    self.btnFiveCross.isHidden = false
                    self.isImageFive = true
                }
            }
            
            self.deleted_document_IDs = self.ImageIdAppendWithComa(ImageArray: images )
            print("Deleted Image IDs : \(self.deleted_document_IDs!)")
            
        }
        
    }
    
    //MARK :- Array Name Comma Seperated
    
    func ImageIdAppendWithComa(ImageArray:[DocumentImages]) -> String {
        
        var imageIdsArray:[String] = [String]()
        for obj in ImageArray {
            let id = "\(obj.id!)"
            imageIdsArray.append(id)
        }
        
        return imageIdsArray.joined(separator: ",")
        
    }
    
    
    @IBAction func documentTypeAction(_ sender : UIButton) {
        self.view.endEditing(true)
        self.educationTypeDropdown.show()
    }
    
    //MARK:-  Education Type Listing Service Calling
    
    func GetEducationList() {
        
        self.view.endEditing(true)
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        UserServices.EducationList(param:[:] , completionHandler: {(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            self.stopLoading()
            print("Response: \(response!)")
            
            if let educationResponseArray = response?["Result"].array {
                
                var educationItem = [String]()
                
                for resultObj in educationResponseArray {
                    let obj =  EducationList(json: resultObj)
                    self.educationTypeArray?.append(obj)
                    if let title = obj.title {
                        educationItem.append((title))
                    }
                }
                
                self.educationTypeDropdown.dataSource = educationItem
                
            }
            
        })
        
    }
    
    
    @IBAction func updateAction(_ sender : UIButton) {
        
        self.view.endEditing(true)
        
        let firstName = self.txtFirstName.text!
        let lastName = self.txtLastName.text!
        let email = self.txtEmail.text!
        let address = self.txtAddress.text!
        let city = self.txtCity.text!
        let guardianName = self.txtGuardianName.text!
        let educationType = self.txtEducationType.text!
        
        if firstName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter first name" , style: .danger)
            return
        }
        
        if lastName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter last name" , style: .danger)
            return
        }
        
        guard let number = self.txtPhone.text, AppHelper.isValidPhone(testStr: number) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid phone number" , style: .danger)
            return
        }
        
        if address.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter address" , style: .danger)
            return
        }
        
        if city.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter city" , style: .danger)
            return
        }
        
        guard let emergencyPhone = self.txtEmergencyPhone.text, AppHelper.isValidPhone(testStr: emergencyPhone) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid emergency phone number" , style: .danger)
            return
        }
        
        if guardianName.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Please enter guardian name" , style: .danger)
            return
        }
        
        guard let guardianPhone = self.txtGuardianPhone.text, AppHelper.isValidPhone(testStr: guardianPhone) else {
            self.showBanner(title: "Error", subTitle: "Please enter valid guardian phone number" , style: .danger)
            return
        }
        
        
        if educationType.isEmptyOrWhitespace() {
            self.showBanner(title: "Error", subTitle: "Kindly select education type" , style: .danger)
            return
        }
        
        self.startLoading(message: "")
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        if self.isImageOne == true {
            self.imageArray.append(self.imgOne.image!)
        }
        if self.isImageTwo == true {
            self.imageArray.append(self.imgTwo.image!)
        }
        if self.isImageThree == true {
            self.imageArray.append(self.imgThree.image!)
        }
        if self.isImageFour == true {
            self.imageArray.append(self.imgFour.image!)
        }
        if self.isImageFive == true {
            self.imageArray.append(self.imgFive.image!)
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        
        let params:[String:Any] = [ "user_id":id , "first_name":firstName , "last_name":lastName , "phone":number , "email":email , "address":address , "city":city , "emergency_contact":emergencyPhone , "guardian_name":guardianName , "guardian_phone":guardianPhone , "education_type":educationType , "document_delete_ids":self.deleted_document_IDs!]
        print("Parameter: \(params)")
        print("Image Count: \(self.imageArray.count)")
        
        UserServices.UpdateProfile(param: params, images: self.imageArray, completionHandler:{(status, response, error) in
            
            if !status {
                if error != nil {
                    let error = String(describing: (error as AnyObject).localizedDescription)
                    print("Error: \(error)")
                    self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg: error , id: 0)
                    self.alertView.isHidden = false
                    self.stopLoading()
                    return
                }
                let msg = response?["Message"].stringValue
                print("Message: \(String(describing: msg))")
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Alert", msg: msg! , id: 0)
                self.alertView.isHidden = false
                self.stopLoading()
                return
            }
            
            print("Response: \(response!)")
            
            let userResultObj = User(object:(response?["Result"])!)
            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
            
            if self.isProfileImage == true {
                self.updateProfileImage()
                return
            }
            
            self.stopLoading()
            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Congratulations", msg: "Your account has been updated successfully", id: 0)
            self.alertView.isHidden = false
            self.textAlert = "Update Profile"
            
        })
        
    }
    
    func updateProfileImage() {
        
        if !AppHelper.isConnectedToInternet() {
            self.showBanner(title: "Error", subTitle: "No Network Connection" , style: .danger)
            self.stopLoading()
            return
        }
        
        let id = Singleton.sharedInstance.CurrentUser!.id!
        let firstName = self.txtFirstName.text!
        let lastName = self.txtLastName.text!
        let email = self.txtEmail.text!
        let address = self.txtAddress.text!
        let city = self.txtCity.text!
        let guardianName = self.txtGuardianName.text!
        let educationType = self.txtEducationType.text!
        let number = self.txtPhone.text!
        let emergencyPhone = self.txtEmergencyPhone.text!
        let guardianPhone = self.txtGuardianPhone.text!
        
        let params:[String:String] = [ "user_id":"\(id)" , "first_name":firstName , "last_name":lastName , "phone":number , "email":email , "address":address , "city":city , "emergency_contact":emergencyPhone , "guardian_name":guardianName , "guardian_phone":guardianPhone , "education_type":educationType , "profile_picture": "file.jpeg"]
        print("Parameter: \(params)")
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append((self.imgProfile.image?.jpegData(compressionQuality: 0.5))!, withName: "profile_picture", fileName: "file.jpeg", mimeType: "image/jpeg")
            for (key, value) in params {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, to:"\(ServiceApiEndPoints.updateProfile)")
        { (result) in
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print("progress::\(progress)")
                })
                upload.responseData(completionHandler: { response in
                    
                    if response.result.isSuccess {
                        if let value: Any = response.result.value as AnyObject? {
                            self.stopLoading()
                            let response = JSON(value)
                            print("Response: \(response)")
                            
                            let userResultObj = User(object:(response["Result"]))
                            UserManager.saveUserObjectToUserDefaults(userResult: userResultObj)
                            Singleton.sharedInstance.CurrentUser = UserManager.getUserObjectFromUserDefaults()
                            
                            self.alertView.alertShow(image: SUCCESS_IMAGE, title: "Congratulations", msg: "Your account has been updated successfully", id: 0)
                            self.alertView.isHidden = false
                            self.textAlert = "Update Profile"
                            
                        }
                    }
                    else {
                        self.stopLoading()
                        print("FAILURE")
                        
                        let error = "\(String(describing: (response.error?.localizedDescription)!))"
                        self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg:error , id: 0)
                        self.alertView.isHidden = false
                    }
                })
                
            case .failure(let encodingError):
                self.stopLoading()
                
                let error = "\(encodingError.localizedDescription))"
                self.alertView.alertShow(image: FAILURE_IMAGE, title: "Error", msg:error , id: 0)
                self.alertView.isHidden = false
                
                break
            }
            
        }
    }
}

extension MyProfileViewController : UINavigationControllerDelegate , UIImagePickerControllerDelegate {
    
    @IBAction func profileImageAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 0)
    }
    
    @IBAction func imageOneAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 1)
    }
    
    @IBAction func imageTwoAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 2)
    }
    
    @IBAction func imageThreeAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 3)
    }
    
    @IBAction func imageFourAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 4)
    }
    
    @IBAction func imageFiveAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.openCamera(id: 5)
    }
    
    func openCamera(id:Int){
        
        let cameraViewController = CameraViewController { [weak self] image, asset in
            if let pickedImage = image {
                
                if id == 0 {
                    
                    self?.isProfileImage = true
                    self?.imgProfile.image = pickedImage
                    self?.imgBackground.image = pickedImage
                    
                }
                else if id == 1 {
                    
                    self?.isImageOne = true
                    self?.imgOne.image = pickedImage
                    
                    self?.imgOneCross.isHidden = false
                    self?.btnOneCross.isHidden = false
                    
                }
                else if id == 2 {
                    
                    self?.isImageTwo = true
                    self?.imgTwo.image = pickedImage
                    
                    self?.imgTwoCross.isHidden = false
                    self?.btnTwoCross.isHidden = false
                    
                }
                else if id == 3 {
                    
                    self?.isImageThree = true
                    self?.imgThree.image = pickedImage
                    
                    self?.imgThreeCross.isHidden = false
                    self?.btnThreeCross.isHidden = false
                    
                }
                else if id == 4 {
                    
                    self?.isImageFour = true
                    self?.imgFour.image = pickedImage
                    
                    self?.imgFourCross.isHidden = false
                    self?.btnFourCross.isHidden = false
                    
                }
                else if id == 5 {
                    
                    self?.isImageFive = true
                    self?.imgFive.image = pickedImage
                    
                    self?.imgFiveCross.isHidden = false
                    self?.btnFiveCross.isHidden = false
                }
                
            }
            
            self?.dismiss(animated: true, completion: nil)
            
        }
        
        present(cameraViewController, animated: true, completion: nil)
    }
    
    @IBAction func CrossOneAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageOne = false
        self.imgOne.image = UIImage(named: "docs_icon")
        
        self.imgOneCross.isHidden = true
        self.btnOneCross.isHidden = true
    }
    
    @IBAction func CrosTwoAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageTwo = false
        self.imgTwo.image = UIImage(named: "docs_icon")
        
        self.imgTwoCross.isHidden = true
        self.btnTwoCross.isHidden = true
        
    }
    
    @IBAction func CrosThreeAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageThree = false
        self.imgThree.image = UIImage(named: "docs_icon")
        
        self.imgThreeCross.isHidden = true
        self.btnThreeCross.isHidden = true
        
    }
    
    @IBAction func CrosFourAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageFour = false
        self.imgFour.image = UIImage(named: "docs_icon")
        
        self.imgFourCross.isHidden = true
        self.btnFourCross.isHidden = true
        
    }
    
    @IBAction func CrosFiveAction(_ sender:UIButton) {
        
        self.view.endEditing(true)
        self.isImageFive = false
        self.imgFive.image = UIImage(named: "docs_icon")
        
        self.imgFiveCross.isHidden = true
        self.btnFiveCross.isHidden = true
    }
    
}






