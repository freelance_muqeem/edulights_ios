//
//  UIColorExtension.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    convenience init(rgb: UInt) {
        
        self.init(
            red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgb & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    static func randomFloat() -> CGFloat {
        return CGFloat(arc4random()) / CGFloat(UInt32.max)
    }
    
    static func random() -> UIColor {
        return UIColor(red:   UIColor.randomFloat(),
                       green: UIColor.randomFloat(),
                       blue:  UIColor.randomFloat(),
                       alpha: 1.0)
    }
}

