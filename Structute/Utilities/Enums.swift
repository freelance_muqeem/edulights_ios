//
//  Enum.swift
//  HireMile
//
//  Created by mac on 5/14/18.
//  Copyright © 2018 mac. All rights reserved.
//

import Foundation

enum BackButton: String {
    case LIGHT
    case DARK
}
