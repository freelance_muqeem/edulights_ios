//
//  Data.swift
//
//  Created by Abdul Muqeem on 10/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class InstituteData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let quota = "quota"
    static let name = "name"
    static let updatedAt = "updated_at"
    static let email = "email"
    static let mobile = "mobile"
    static let address = "address"
    static let descriptionValue = "description"
    static let logoImage = "logo_image"
    static let status = "status"
    static let id = "id"
    static let isDeleted = "is_deleted"
    static let password = "password"
    static let createdAt = "created_at"
    static let logo = "logo"
  }

  // MARK: Properties
  public var quota: String?
  public var name: String?
  public var updatedAt: String?
  public var email: String?
  public var mobile: String?
  public var address: String?
  public var descriptionValue: String?
  public var logoImage: String?
  public var status: String?
  public var id: Int?
  public var isDeleted: String?
  public var password: String?
  public var createdAt: String?
  public var logo: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    quota = json[SerializationKeys.quota].string
    name = json[SerializationKeys.name].string
    updatedAt = json[SerializationKeys.updatedAt].string
    email = json[SerializationKeys.email].string
    mobile = json[SerializationKeys.mobile].string
    address = json[SerializationKeys.address].string
    descriptionValue = json[SerializationKeys.descriptionValue].string
    logoImage = json[SerializationKeys.logoImage].string
    status = json[SerializationKeys.status].string
    id = json[SerializationKeys.id].int
    isDeleted = json[SerializationKeys.isDeleted].string
    password = json[SerializationKeys.password].string
    createdAt = json[SerializationKeys.createdAt].string
    logo = json[SerializationKeys.logo].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = quota { dictionary[SerializationKeys.quota] = value }
    if let value = name { dictionary[SerializationKeys.name] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = email { dictionary[SerializationKeys.email] = value }
    if let value = mobile { dictionary[SerializationKeys.mobile] = value }
    if let value = address { dictionary[SerializationKeys.address] = value }
    if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
    if let value = logoImage { dictionary[SerializationKeys.logoImage] = value }
    if let value = status { dictionary[SerializationKeys.status] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = isDeleted { dictionary[SerializationKeys.isDeleted] = value }
    if let value = password { dictionary[SerializationKeys.password] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = logo { dictionary[SerializationKeys.logo] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.quota = aDecoder.decodeObject(forKey: SerializationKeys.quota) as? String
    self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
    self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
    self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
    self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
    self.logoImage = aDecoder.decodeObject(forKey: SerializationKeys.logoImage) as? String
    self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.isDeleted = aDecoder.decodeObject(forKey: SerializationKeys.isDeleted) as? String
    self.password = aDecoder.decodeObject(forKey: SerializationKeys.password) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.logo = aDecoder.decodeObject(forKey: SerializationKeys.logo) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(quota, forKey: SerializationKeys.quota)
    aCoder.encode(name, forKey: SerializationKeys.name)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(email, forKey: SerializationKeys.email)
    aCoder.encode(mobile, forKey: SerializationKeys.mobile)
    aCoder.encode(address, forKey: SerializationKeys.address)
    aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
    aCoder.encode(logoImage, forKey: SerializationKeys.logoImage)
    aCoder.encode(status, forKey: SerializationKeys.status)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
    aCoder.encode(password, forKey: SerializationKeys.password)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(logo, forKey: SerializationKeys.logo)
  }

}
