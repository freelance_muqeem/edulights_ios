//
//  Result.swift
//
//  Created by Abdul Muqeem on 18/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class NotificationData: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let actionId = "action_id"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let recieverId = "reciever_id"
    static let senderId = "sender_id"
    static let createdAt = "created_at"
    static let message = "message"
    static let actionType = "action_type"
  }

  // MARK: Properties
  public var actionId: String?
  public var updatedAt: String?
  public var id: Int?
  public var recieverId: String?
  public var senderId: String?
  public var createdAt: String?
  public var message: String?
  public var actionType: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    actionId = json[SerializationKeys.actionId].string
    updatedAt = json[SerializationKeys.updatedAt].string
    id = json[SerializationKeys.id].int
    recieverId = json[SerializationKeys.recieverId].string
    senderId = json[SerializationKeys.senderId].string
    createdAt = json[SerializationKeys.createdAt].string
    message = json[SerializationKeys.message].string
    actionType = json[SerializationKeys.actionType].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = actionId { dictionary[SerializationKeys.actionId] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = recieverId { dictionary[SerializationKeys.recieverId] = value }
    if let value = senderId { dictionary[SerializationKeys.senderId] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = message { dictionary[SerializationKeys.message] = value }
    if let value = actionType { dictionary[SerializationKeys.actionType] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.actionId = aDecoder.decodeObject(forKey: SerializationKeys.actionId) as? String
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.recieverId = aDecoder.decodeObject(forKey: SerializationKeys.recieverId) as? String
    self.senderId = aDecoder.decodeObject(forKey: SerializationKeys.senderId) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.message = aDecoder.decodeObject(forKey: SerializationKeys.message) as? String
    self.actionType = aDecoder.decodeObject(forKey: SerializationKeys.actionType) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(actionId, forKey: SerializationKeys.actionId)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(recieverId, forKey: SerializationKeys.recieverId)
    aCoder.encode(senderId, forKey: SerializationKeys.senderId)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(message, forKey: SerializationKeys.message)
    aCoder.encode(actionType, forKey: SerializationKeys.actionType)
  }

}
