//
//  DocumentImages.swift
//
//  Created by Abdul Muqeem on 21/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class DocumentImages: NSObject , NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let userId = "user_id"
    static let id = "id"
    static let updatedAt = "updated_at"
    static let document = "document"
    static let createdAt = "created_at"
  }

  // MARK: Properties
  public var userId: String?
  public var id: Int?
  public var updatedAt: String?
  public var document: String?
  public var createdAt: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    userId = json[SerializationKeys.userId].string
    id = json[SerializationKeys.id].int
    updatedAt = json[SerializationKeys.updatedAt].string
    document = json[SerializationKeys.document].string
    createdAt = json[SerializationKeys.createdAt].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = userId { dictionary[SerializationKeys.userId] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = document { dictionary[SerializationKeys.document] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.userId = aDecoder.decodeObject(forKey: SerializationKeys.userId) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.document = aDecoder.decodeObject(forKey: SerializationKeys.document) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(userId, forKey: SerializationKeys.userId)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(document, forKey: SerializationKeys.document)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
  }

}
