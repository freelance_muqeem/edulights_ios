//
//  Body.swift
//
//  Created by Abdul Muqeem on 25/12/2018
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class User: NSObject , NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let documentImages = "document_images"
        static let profileImage = "profile_image"
        static let isVerify = "is_verify"
        static let bio = "bio"
        static let verifyToken = "verify_token"
        static let lastName = "last_name"
        static let guardianName = "guardian_name"
        static let id = "id"
        static let deviceToken = "device_token"
        static let phone = "phone"
        static let profilePicture = "profile_picture"
        static let city = "city"
        static let emergencyContact = "emergency_contact"
        static let email = "email"
        static let socialMediaId = "social_media_id"
        static let fullName = "full_name"
        static let address = "address"
        static let deviceType = "device_type"
        static let status = "status"
        static let guardianPhone = "guardian_phone"
        static let roleId = "role_id"
        static let firstName = "first_name"
        static let notificationStatus = "notification_status"
        static let educationType = "education_type"
        static let rights = "rights"
        static let token = "_token"
    }
    
    // MARK: Properties
    public var documentImages: [DocumentImages]?
    public var profileImage: String?
    public var isVerify: Int?
    public var bio: String?
    public var verifyToken: String?
    public var lastName: String?
    public var guardianName: String?
    public var id: Int?
    public var deviceToken: String?
    public var phone: String?
    public var profilePicture: String?
    public var city: String?
    public var emergencyContact: String?
    public var email: String?
    public var socialMediaId: Int?
    public var fullName: String?
    public var address: String?
    public var deviceType: String?
    public var status: Int?
    public var guardianPhone: String?
    public var roleId: Int?
    public var firstName: String?
    public var notificationStatus: Int?
    public var educationType: String?
    public var rights: String?
    public var token: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        if let items = json[SerializationKeys.documentImages].array { documentImages = items.map { DocumentImages(json: $0) } }
        profileImage = json[SerializationKeys.profileImage].string
        isVerify = json[SerializationKeys.isVerify].int
        bio = json[SerializationKeys.bio].string
        verifyToken = json[SerializationKeys.verifyToken].string
        lastName = json[SerializationKeys.lastName].string
        guardianName = json[SerializationKeys.guardianName].string
        id = json[SerializationKeys.id].int
        deviceToken = json[SerializationKeys.deviceToken].string
        phone = json[SerializationKeys.phone].string
        profilePicture = json[SerializationKeys.profilePicture].string
        city = json[SerializationKeys.city].string
        emergencyContact = json[SerializationKeys.emergencyContact].string
        email = json[SerializationKeys.email].string
        socialMediaId = json[SerializationKeys.socialMediaId].int
        fullName = json[SerializationKeys.fullName].string
        address = json[SerializationKeys.address].string
        deviceType = json[SerializationKeys.deviceType].string
        status = json[SerializationKeys.status].int
        guardianPhone = json[SerializationKeys.guardianPhone].string
        roleId = json[SerializationKeys.roleId].int
        firstName = json[SerializationKeys.firstName].string
        notificationStatus = json[SerializationKeys.notificationStatus].int
        educationType = json[SerializationKeys.educationType].string
        rights = json[SerializationKeys.rights].string
        token = json[SerializationKeys.token].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = documentImages { dictionary[SerializationKeys.documentImages] = value.map { $0.dictionaryRepresentation() } }
        if let value = profileImage { dictionary[SerializationKeys.profileImage] = value }
        if let value = isVerify { dictionary[SerializationKeys.isVerify] = value }
        if let value = bio { dictionary[SerializationKeys.bio] = value }
        if let value = verifyToken { dictionary[SerializationKeys.verifyToken] = value }
        if let value = lastName { dictionary[SerializationKeys.lastName] = value }
        if let value = guardianName { dictionary[SerializationKeys.guardianName] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = deviceToken { dictionary[SerializationKeys.deviceToken] = value }
        if let value = phone { dictionary[SerializationKeys.phone] = value }
        if let value = profilePicture { dictionary[SerializationKeys.profilePicture] = value }
        if let value = city { dictionary[SerializationKeys.city] = value }
        if let value = emergencyContact { dictionary[SerializationKeys.emergencyContact] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = socialMediaId { dictionary[SerializationKeys.socialMediaId] = value }
        if let value = fullName { dictionary[SerializationKeys.fullName] = value }
        if let value = address { dictionary[SerializationKeys.address] = value }
        if let value = deviceType { dictionary[SerializationKeys.deviceType] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = guardianPhone { dictionary[SerializationKeys.guardianPhone] = value }
        if let value = roleId { dictionary[SerializationKeys.roleId] = value }
        if let value = firstName { dictionary[SerializationKeys.firstName] = value }
        if let value = notificationStatus { dictionary[SerializationKeys.notificationStatus] = value }
        if let value = educationType { dictionary[SerializationKeys.educationType] = value }
        if let value = rights { dictionary[SerializationKeys.rights] = value }
        if let value = token { dictionary[SerializationKeys.token] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.documentImages = aDecoder.decodeObject(forKey: SerializationKeys.documentImages) as? [DocumentImages]
        self.profileImage = aDecoder.decodeObject(forKey: SerializationKeys.profileImage) as? String
        self.isVerify = aDecoder.decodeObject(forKey: SerializationKeys.isVerify) as? Int
        self.bio = aDecoder.decodeObject(forKey: SerializationKeys.bio) as? String
        self.verifyToken = aDecoder.decodeObject(forKey: SerializationKeys.verifyToken) as? String
        self.lastName = aDecoder.decodeObject(forKey: SerializationKeys.lastName) as? String
        self.guardianName = aDecoder.decodeObject(forKey: SerializationKeys.guardianName) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.deviceToken = aDecoder.decodeObject(forKey: SerializationKeys.deviceToken) as? String
        self.phone = aDecoder.decodeObject(forKey: SerializationKeys.phone) as? String
        self.profilePicture = aDecoder.decodeObject(forKey: SerializationKeys.profilePicture) as? String
        self.city = aDecoder.decodeObject(forKey: SerializationKeys.city) as? String
        self.emergencyContact = aDecoder.decodeObject(forKey: SerializationKeys.emergencyContact) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.socialMediaId = aDecoder.decodeObject(forKey: SerializationKeys.socialMediaId) as? Int
        self.fullName = aDecoder.decodeObject(forKey: SerializationKeys.fullName) as? String
        self.address = aDecoder.decodeObject(forKey: SerializationKeys.address) as? String
        self.deviceType = aDecoder.decodeObject(forKey: SerializationKeys.deviceType) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? Int
        self.guardianPhone = aDecoder.decodeObject(forKey: SerializationKeys.guardianPhone) as? String
        self.roleId = aDecoder.decodeObject(forKey: SerializationKeys.roleId) as? Int
        self.firstName = aDecoder.decodeObject(forKey: SerializationKeys.firstName) as? String
        self.notificationStatus = aDecoder.decodeObject(forKey: SerializationKeys.notificationStatus) as? Int
        self.educationType = aDecoder.decodeObject(forKey: SerializationKeys.educationType) as? String
        self.rights = aDecoder.decodeObject(forKey: SerializationKeys.rights) as? String
        self.token = aDecoder.decodeObject(forKey: SerializationKeys.token) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(documentImages, forKey: SerializationKeys.documentImages)
        aCoder.encode(profileImage, forKey: SerializationKeys.profileImage)
        aCoder.encode(isVerify, forKey: SerializationKeys.isVerify)
        aCoder.encode(bio, forKey: SerializationKeys.bio)
        aCoder.encode(verifyToken, forKey: SerializationKeys.verifyToken)
        aCoder.encode(lastName, forKey: SerializationKeys.lastName)
        aCoder.encode(guardianName, forKey: SerializationKeys.guardianName)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(deviceToken, forKey: SerializationKeys.deviceToken)
        aCoder.encode(phone, forKey: SerializationKeys.phone)
        aCoder.encode(profilePicture, forKey: SerializationKeys.profilePicture)
        aCoder.encode(city, forKey: SerializationKeys.city)
        aCoder.encode(emergencyContact, forKey: SerializationKeys.emergencyContact)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(socialMediaId, forKey: SerializationKeys.socialMediaId)
        aCoder.encode(fullName, forKey: SerializationKeys.fullName)
        aCoder.encode(address, forKey: SerializationKeys.address)
        aCoder.encode(deviceType, forKey: SerializationKeys.deviceType)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(guardianPhone, forKey: SerializationKeys.guardianPhone)
        aCoder.encode(roleId, forKey: SerializationKeys.roleId)
        aCoder.encode(firstName, forKey: SerializationKeys.firstName)
        aCoder.encode(notificationStatus, forKey: SerializationKeys.notificationStatus)
        aCoder.encode(educationType, forKey: SerializationKeys.educationType)
        aCoder.encode(rights, forKey: SerializationKeys.rights)
        aCoder.encode(token, forKey: SerializationKeys.token)
    }
    
}
