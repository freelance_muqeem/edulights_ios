//
//  Batchdays.swift
//
//  Created by Abdul Muqeem on 11/11/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Batchdays: NSCoding {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let day = "day"
    static let createdAt = "created_at"
    static let fromTime = "from_time"
    static let batchId = "batch_id"
    static let toTime = "to_time"
  }

  // MARK: Properties
  public var updatedAt: String?
  public var id: Int?
  public var day: String?
  public var createdAt: String?
  public var fromTime: String?
  public var batchId: String?
  public var toTime: String?

  // MARK: SwiftyJSON Initializers
  /// Initiates the instance based on the object.
  ///
  /// - parameter object: The object of either Dictionary or Array kind that was passed.
  /// - returns: An initialized instance of the class.
  public convenience init(object: Any) {
    self.init(json: JSON(object))
  }

  /// Initiates the instance based on the JSON that was passed.
  ///
  /// - parameter json: JSON object from SwiftyJSON.
  public required init(json: JSON) {
    updatedAt = json[SerializationKeys.updatedAt].string
    id = json[SerializationKeys.id].int
    day = json[SerializationKeys.day].string
    createdAt = json[SerializationKeys.createdAt].string
    fromTime = json[SerializationKeys.fromTime].string
    batchId = json[SerializationKeys.batchId].string
    toTime = json[SerializationKeys.toTime].string
  }

  /// Generates description of the object in the form of a NSDictionary.
  ///
  /// - returns: A Key value pair containing all valid values in the object.
  public func dictionaryRepresentation() -> [String: Any] {
    var dictionary: [String: Any] = [:]
    if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
    if let value = id { dictionary[SerializationKeys.id] = value }
    if let value = day { dictionary[SerializationKeys.day] = value }
    if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
    if let value = fromTime { dictionary[SerializationKeys.fromTime] = value }
    if let value = batchId { dictionary[SerializationKeys.batchId] = value }
    if let value = toTime { dictionary[SerializationKeys.toTime] = value }
    return dictionary
  }

  // MARK: NSCoding Protocol
  required public init(coder aDecoder: NSCoder) {
    self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
    self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
    self.day = aDecoder.decodeObject(forKey: SerializationKeys.day) as? String
    self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
    self.fromTime = aDecoder.decodeObject(forKey: SerializationKeys.fromTime) as? String
    self.batchId = aDecoder.decodeObject(forKey: SerializationKeys.batchId) as? String
    self.toTime = aDecoder.decodeObject(forKey: SerializationKeys.toTime) as? String
  }

  public func encode(with aCoder: NSCoder) {
    aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
    aCoder.encode(id, forKey: SerializationKeys.id)
    aCoder.encode(day, forKey: SerializationKeys.day)
    aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
    aCoder.encode(fromTime, forKey: SerializationKeys.fromTime)
    aCoder.encode(batchId, forKey: SerializationKeys.batchId)
    aCoder.encode(toTime, forKey: SerializationKeys.toTime)
  }

}
