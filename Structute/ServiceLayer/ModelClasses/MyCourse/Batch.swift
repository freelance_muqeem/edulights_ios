//
//  Result.swift
//
//  Created by Abdul Muqeem on 19/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Batch: NSCoding {

      // MARK: Declaration for string constants to be used to decode and also serialize.
      private struct SerializationKeys {
        static let batchNo = "batch_no"
        static let batchdays = "batchdays"
        static let noOfStudent = "no_of_student"
        static let updatedAt = "updated_at"
        static let id = "id"
        static let betchFromTime = "betch_from_time"
        static let day = "day"
        static let createdAt = "created_at"
        static let betchToTime = "betch_to_time"
        static let endDate = "end_date"
        static let startDate = "start_date"
        static let coursesId = "courses_id"
      }

      // MARK: Properties
      public var batchNo: String?
      public var batchdays: [Batchdays]?
      public var noOfStudent: String?
      public var updatedAt: String?
      public var id: Int?
      public var betchFromTime: String?
      public var day: String?
      public var createdAt: String?
      public var betchToTime: String?
      public var endDate: String?
      public var startDate: String?
      public var coursesId: String?

      // MARK: SwiftyJSON Initializers
      /// Initiates the instance based on the object.
      ///
      /// - parameter object: The object of either Dictionary or Array kind that was passed.
      /// - returns: An initialized instance of the class.
      public convenience init(object: Any) {
        self.init(json: JSON(object))
      }

      /// Initiates the instance based on the JSON that was passed.
      ///
      /// - parameter json: JSON object from SwiftyJSON.
      public required init(json: JSON) {
        batchNo = json[SerializationKeys.batchNo].string
        if let items = json[SerializationKeys.batchdays].array { batchdays = items.map { Batchdays(json: $0) } }
        noOfStudent = json[SerializationKeys.noOfStudent].string
        updatedAt = json[SerializationKeys.updatedAt].string
        id = json[SerializationKeys.id].int
        betchFromTime = json[SerializationKeys.betchFromTime].string
        day = json[SerializationKeys.day].string
        createdAt = json[SerializationKeys.createdAt].string
        betchToTime = json[SerializationKeys.betchToTime].string
        endDate = json[SerializationKeys.endDate].string
        startDate = json[SerializationKeys.startDate].string
        coursesId = json[SerializationKeys.coursesId].string
      }

      /// Generates description of the object in the form of a NSDictionary.
      ///
      /// - returns: A Key value pair containing all valid values in the object.
      public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = batchNo { dictionary[SerializationKeys.batchNo] = value }
        if let value = batchdays { dictionary[SerializationKeys.batchdays] = value.map { $0.dictionaryRepresentation() } }
        if let value = noOfStudent { dictionary[SerializationKeys.noOfStudent] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = betchFromTime { dictionary[SerializationKeys.betchFromTime] = value }
        if let value = day { dictionary[SerializationKeys.day] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = betchToTime { dictionary[SerializationKeys.betchToTime] = value }
        if let value = endDate { dictionary[SerializationKeys.endDate] = value }
        if let value = startDate { dictionary[SerializationKeys.startDate] = value }
        if let value = coursesId { dictionary[SerializationKeys.coursesId] = value }
        return dictionary
      }

      // MARK: NSCoding Protocol
      required public init(coder aDecoder: NSCoder) {
        self.batchNo = aDecoder.decodeObject(forKey: SerializationKeys.batchNo) as? String
        self.batchdays = aDecoder.decodeObject(forKey: SerializationKeys.batchdays) as? [Batchdays]
        self.noOfStudent = aDecoder.decodeObject(forKey: SerializationKeys.noOfStudent) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.betchFromTime = aDecoder.decodeObject(forKey: SerializationKeys.betchFromTime) as? String
        self.day = aDecoder.decodeObject(forKey: SerializationKeys.day) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.betchToTime = aDecoder.decodeObject(forKey: SerializationKeys.betchToTime) as? String
        self.endDate = aDecoder.decodeObject(forKey: SerializationKeys.endDate) as? String
        self.startDate = aDecoder.decodeObject(forKey: SerializationKeys.startDate) as? String
        self.coursesId = aDecoder.decodeObject(forKey: SerializationKeys.coursesId) as? String
      }

      public func encode(with aCoder: NSCoder) {
        aCoder.encode(batchNo, forKey: SerializationKeys.batchNo)
        aCoder.encode(batchdays, forKey: SerializationKeys.batchdays)
        aCoder.encode(noOfStudent, forKey: SerializationKeys.noOfStudent)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(betchFromTime, forKey: SerializationKeys.betchFromTime)
        aCoder.encode(day, forKey: SerializationKeys.day)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(betchToTime, forKey: SerializationKeys.betchToTime)
        aCoder.encode(endDate, forKey: SerializationKeys.endDate)
        aCoder.encode(startDate, forKey: SerializationKeys.startDate)
        aCoder.encode(coursesId, forKey: SerializationKeys.coursesId)
      }

    }
