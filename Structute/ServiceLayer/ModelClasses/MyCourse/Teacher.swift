//
//  Teacher.swift
//
//  Created by Abdul Muqeem on 18/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Teacher: NSCoding {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let updatedAt = "updated_at"
        static let email = "email"
        static let mobile = "mobile"
        static let descriptionValue = "description"
        static let profileImage = "profile_image"
        static let courses = "courses"
        static let instituteId = "institute_id"
        static let qualification = "qualification"
        static let status = "status"
        static let id = "id"
        static let image = "image"
        static let isDeleted = "is_deleted"
        static let createdAt = "created_at"
        static let approvalStatus = "approval_status"
    }
    
    // MARK: Properties
    public var name: String?
    public var updatedAt: String?
    public var email: String?
    public var mobile: String?
    public var descriptionValue: String?
    public var profileImage: String?
    public var courses: [TeacherCourses]?
    public var instituteId: String?
    public var qualification: String?
    public var status: String?
    public var id: Int?
    public var image: String?
    public var isDeleted: String?
    public var createdAt: String?
    public var approvalStatus: String?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        name = json[SerializationKeys.name].string
        updatedAt = json[SerializationKeys.updatedAt].string
        email = json[SerializationKeys.email].string
        mobile = json[SerializationKeys.mobile].string
        descriptionValue = json[SerializationKeys.descriptionValue].string
        profileImage = json[SerializationKeys.profileImage].string
        if let items = json[SerializationKeys.courses].array { courses = items.map { TeacherCourses(json: $0) } }
        instituteId = json[SerializationKeys.instituteId].string
        qualification = json[SerializationKeys.qualification].string
        status = json[SerializationKeys.status].string
        id = json[SerializationKeys.id].int
        image = json[SerializationKeys.image].string
        isDeleted = json[SerializationKeys.isDeleted].string
        createdAt = json[SerializationKeys.createdAt].string
        approvalStatus = json[SerializationKeys.approvalStatus].string
    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = email { dictionary[SerializationKeys.email] = value }
        if let value = mobile { dictionary[SerializationKeys.mobile] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = profileImage { dictionary[SerializationKeys.profileImage] = value }
        if let value = courses { dictionary[SerializationKeys.courses] = value.map { $0.dictionaryRepresentation() } }
        if let value = instituteId { dictionary[SerializationKeys.instituteId] = value }
        if let value = qualification { dictionary[SerializationKeys.qualification] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = image { dictionary[SerializationKeys.image] = value }
        if let value = isDeleted { dictionary[SerializationKeys.isDeleted] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = approvalStatus { dictionary[SerializationKeys.approvalStatus] = value }
        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.email = aDecoder.decodeObject(forKey: SerializationKeys.email) as? String
        self.mobile = aDecoder.decodeObject(forKey: SerializationKeys.mobile) as? String
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.profileImage = aDecoder.decodeObject(forKey: SerializationKeys.profileImage) as? String
        self.courses = aDecoder.decodeObject(forKey: SerializationKeys.courses) as? [TeacherCourses]
        self.instituteId = aDecoder.decodeObject(forKey: SerializationKeys.instituteId) as? String
        self.qualification = aDecoder.decodeObject(forKey: SerializationKeys.qualification) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.image = aDecoder.decodeObject(forKey: SerializationKeys.image) as? String
        self.isDeleted = aDecoder.decodeObject(forKey: SerializationKeys.isDeleted) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.approvalStatus = aDecoder.decodeObject(forKey: SerializationKeys.approvalStatus) as? String
    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(email, forKey: SerializationKeys.email)
        aCoder.encode(mobile, forKey: SerializationKeys.mobile)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(profileImage, forKey: SerializationKeys.profileImage)
        aCoder.encode(courses, forKey: SerializationKeys.courses)
        aCoder.encode(instituteId, forKey: SerializationKeys.instituteId)
        aCoder.encode(qualification, forKey: SerializationKeys.qualification)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(image, forKey: SerializationKeys.image)
        aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(approvalStatus, forKey: SerializationKeys.approvalStatus)
    }
    
}
