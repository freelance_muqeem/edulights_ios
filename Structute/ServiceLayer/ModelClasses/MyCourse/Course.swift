//
//  Result.swift
//
//  Created by Abdul Muqeem on 10/05/2019
//  Copyright (c) . All rights reserved.
//

import Foundation
import SwiftyJSON

public final class Course: NSCoding {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let institute = "institute"
        static let teacher = "teacher"
        static let name = "name"
        static let updatedAt = "updated_at"
        static let minimumEducation = "minimum_education"
        static let descriptionValue = "description"
        static let fees = "fees"
        static let aptitude = "aptitude"
        static let instituteId = "institute_id"
        static let teacherId = "teacher_id"
        static let status = "status"
        static let id = "id"
        static let noOfBatches = "no_of_batches"
        static let isDeleted = "is_deleted"
        static let createdAt = "created_at"
        static let num = "num"
        static let minimumAttendance = "minimum_attendance"
        static let additionalDetail = "additional_detail"
        static let startDate = "start_date"
        static let endDate = "end_date"
        static let batches = "batches"
    }
    
    // MARK: Properties
    public var institute: Institute?
    public var teacher: Teacher?
    public var name: String?
    public var updatedAt: String?
    public var minimumEducation: String?
    public var descriptionValue: String?
    public var fees: String?
    public var aptitude: String?
    public var instituteId: String?
    public var teacherId: String?
    public var status: String?
    public var id: Int?
    public var noOfBatches: String?
    public var isDeleted: String?
    public var createdAt: String?
    public var num: Int?
    public var minimumAttendance: String?
    public var additionalDetail: String?
    public var startDate: String?
    public var endDate: String?
    public var batches: [Batch]?
    
    // MARK: SwiftyJSON Initializers
    /// Initiates the instance based on the object.
    ///
    /// - parameter object: The object of either Dictionary or Array kind that was passed.
    /// - returns: An initialized instance of the class.
    public convenience init(object: Any) {
        self.init(json: JSON(object))
    }
    
    /// Initiates the instance based on the JSON that was passed.
    ///
    /// - parameter json: JSON object from SwiftyJSON.
    public required init(json: JSON) {
        institute = Institute(json: json[SerializationKeys.institute])
        teacher = Teacher(json: json[SerializationKeys.teacher])
        name = json[SerializationKeys.name].string
        updatedAt = json[SerializationKeys.updatedAt].string
        minimumEducation = json[SerializationKeys.minimumEducation].string
        descriptionValue = json[SerializationKeys.descriptionValue].string
        fees = json[SerializationKeys.fees].string
        aptitude = json[SerializationKeys.aptitude].string
        instituteId = json[SerializationKeys.instituteId].string
        teacherId = json[SerializationKeys.teacherId].string
        status = json[SerializationKeys.status].string
        id = json[SerializationKeys.id].int
        noOfBatches = json[SerializationKeys.noOfBatches].string
        isDeleted = json[SerializationKeys.isDeleted].string
        createdAt = json[SerializationKeys.createdAt].string
        num = json[SerializationKeys.num].int
        minimumAttendance = json[SerializationKeys.minimumAttendance].string
        additionalDetail = json[SerializationKeys.additionalDetail].string
        startDate = json[SerializationKeys.startDate].string
        endDate = json[SerializationKeys.endDate].string
        if let items = json[SerializationKeys.batches].array { batches = items.map { Batch(json: $0) } }

    }
    
    /// Generates description of the object in the form of a NSDictionary.
    ///
    /// - returns: A Key value pair containing all valid values in the object.
    public func dictionaryRepresentation() -> [String: Any] {
        var dictionary: [String: Any] = [:]
        if let value = institute { dictionary[SerializationKeys.institute] = value.dictionaryRepresentation() }
        if let value = teacher { dictionary[SerializationKeys.teacher] = value.dictionaryRepresentation() }
        if let value = name { dictionary[SerializationKeys.name] = value }
        if let value = updatedAt { dictionary[SerializationKeys.updatedAt] = value }
        if let value = minimumEducation { dictionary[SerializationKeys.minimumEducation] = value }
        if let value = descriptionValue { dictionary[SerializationKeys.descriptionValue] = value }
        if let value = fees { dictionary[SerializationKeys.fees] = value }
        if let value = aptitude { dictionary[SerializationKeys.aptitude] = value }
        if let value = instituteId { dictionary[SerializationKeys.instituteId] = value }
        if let value = teacherId { dictionary[SerializationKeys.teacherId] = value }
        if let value = status { dictionary[SerializationKeys.status] = value }
        if let value = id { dictionary[SerializationKeys.id] = value }
        if let value = noOfBatches { dictionary[SerializationKeys.noOfBatches] = value }
        if let value = isDeleted { dictionary[SerializationKeys.isDeleted] = value }
        if let value = createdAt { dictionary[SerializationKeys.createdAt] = value }
        if let value = num { dictionary[SerializationKeys.num] = value }
        if let value = minimumAttendance { dictionary[SerializationKeys.minimumAttendance] = value }
        if let value = additionalDetail { dictionary[SerializationKeys.additionalDetail] = value }
        if let value = startDate { dictionary[SerializationKeys.startDate] = value }
        if let value = endDate { dictionary[SerializationKeys.endDate] = value }
        if let value = batches { dictionary[SerializationKeys.batches] = value.map { $0.dictionaryRepresentation() } }

        return dictionary
    }
    
    // MARK: NSCoding Protocol
    required public init(coder aDecoder: NSCoder) {
        self.institute = aDecoder.decodeObject(forKey: SerializationKeys.institute) as? Institute
        self.teacher = aDecoder.decodeObject(forKey: SerializationKeys.teacher) as? Teacher
        self.name = aDecoder.decodeObject(forKey: SerializationKeys.name) as? String
        self.updatedAt = aDecoder.decodeObject(forKey: SerializationKeys.updatedAt) as? String
        self.minimumEducation = aDecoder.decodeObject(forKey: SerializationKeys.minimumEducation) as? String
        self.descriptionValue = aDecoder.decodeObject(forKey: SerializationKeys.descriptionValue) as? String
        self.fees = aDecoder.decodeObject(forKey: SerializationKeys.fees) as? String
        self.aptitude = aDecoder.decodeObject(forKey: SerializationKeys.aptitude) as? String
        self.instituteId = aDecoder.decodeObject(forKey: SerializationKeys.instituteId) as? String
        self.teacherId = aDecoder.decodeObject(forKey: SerializationKeys.teacherId) as? String
        self.status = aDecoder.decodeObject(forKey: SerializationKeys.status) as? String
        self.id = aDecoder.decodeObject(forKey: SerializationKeys.id) as? Int
        self.noOfBatches = aDecoder.decodeObject(forKey: SerializationKeys.noOfBatches) as? String
        self.isDeleted = aDecoder.decodeObject(forKey: SerializationKeys.isDeleted) as? String
        self.createdAt = aDecoder.decodeObject(forKey: SerializationKeys.createdAt) as? String
        self.num = aDecoder.decodeObject(forKey: SerializationKeys.num) as? Int
        self.minimumAttendance = aDecoder.decodeObject(forKey: SerializationKeys.minimumAttendance) as? String
        self.additionalDetail = aDecoder.decodeObject(forKey: SerializationKeys.additionalDetail) as? String
        self.startDate = aDecoder.decodeObject(forKey: SerializationKeys.startDate) as? String
        self.endDate = aDecoder.decodeObject(forKey: SerializationKeys.endDate) as? String
        self.batches = aDecoder.decodeObject(forKey: SerializationKeys.batches) as? [Batch]

    }
    
    public func encode(with aCoder: NSCoder) {
        aCoder.encode(institute, forKey: SerializationKeys.institute)
        aCoder.encode(teacher, forKey: SerializationKeys.teacher)
        aCoder.encode(name, forKey: SerializationKeys.name)
        aCoder.encode(updatedAt, forKey: SerializationKeys.updatedAt)
        aCoder.encode(minimumEducation, forKey: SerializationKeys.minimumEducation)
        aCoder.encode(descriptionValue, forKey: SerializationKeys.descriptionValue)
        aCoder.encode(fees, forKey: SerializationKeys.fees)
        aCoder.encode(aptitude, forKey: SerializationKeys.aptitude)
        aCoder.encode(instituteId, forKey: SerializationKeys.instituteId)
        aCoder.encode(teacherId, forKey: SerializationKeys.teacherId)
        aCoder.encode(status, forKey: SerializationKeys.status)
        aCoder.encode(id, forKey: SerializationKeys.id)
        aCoder.encode(noOfBatches, forKey: SerializationKeys.noOfBatches)
        aCoder.encode(isDeleted, forKey: SerializationKeys.isDeleted)
        aCoder.encode(createdAt, forKey: SerializationKeys.createdAt)
        aCoder.encode(num, forKey: SerializationKeys.num)
        aCoder.encode(minimumAttendance, forKey: SerializationKeys.minimumAttendance)
        aCoder.encode(additionalDetail, forKey: SerializationKeys.additionalDetail)
        aCoder.encode(startDate, forKey: SerializationKeys.startDate)
        aCoder.encode(endDate, forKey: SerializationKeys.endDate)
        aCoder.encode(batches, forKey: SerializationKeys.batches)
    }
    
}
